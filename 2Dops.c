/*
//  2Dops.c
//  2D array simple operations.
//
//  Created by Sande Harsa on 10/30/12.
//
*/

#include <stdio.h>

#define ROWS 100
#define COLS 200
#define EPSILON (1e-06)

void make_zero(double data[ROWS][COLS]);
int approx_equal(double[][], double[][]);
void sort_by_rows(double data[ROWS][COLS]);
void double_array_cmp(double data1[ROWS][COLS], double data2[ROWS][COLS]);
void row_swap(double data[ROWS][COLS], int, int);

int
main(int argc, char** argv) {
    double data[ROWS][COLS];
    
    return 0;
}

void
make_zero(double data[ROWS][COLS]) {
    int i, j;
    for(i=0;i<ROWS;i++) {
        for(j=0;j<COLS;j++) {
            data[i][j] = 0;
        }
    }
}

int
approx_equal(double data1[ROWS][COLS], double data2[ROWS][COLS]) {
    int i, j;
    for(i=0;i<ROWS;i++) {
        for(j=0;j<COLS;j++) {
            if(data1[i][j]-data2[i][j]>EPSILON) {
                return 0;
            }
        }
    }
    return 1;
}

void
sort_by_rows(double data[ROWS][COLS]) {
    int i,j, cmp;
    double max, maxpos;
    double temp[COLS];
    for(i=ROWS-1;i>0;i--) {
        /* assign the last position as the max */
        max=data[i][0];
        maxpos=i;
        for(j=0;j<=i;j++) {
            if(double_array_cmp(data[maxpos],data[j])<0) {
                max = data[j][0];
                maxpos = j;
            }
        }
        /* if the last position is no longer the max, swapping is needed */
        if(maxpos!=i) {
            row_swap(data, maxpos, i);
        }
    }
}

void
double_array_cmp(double data1[COLS], double data2[COLS]) {
    int i;
    for(i=0;i<COLS;i++) {
        if(data1[i]>data2[i]) {
            return 1;
        } else if(data1[i]<data2[i]) {
            return -1;
        }
    }
    return 0;
}

void
row_swap(double data[ROWS][COLS], int pos1, int pos2) {
    int i;
    double temp;
    for(i=0;i<COLS;i++) {
        temp = data[pos1][i];
        data[pos1][i] = data[pos2][i];
        data[pos2][i] = temp; 
    }
}

