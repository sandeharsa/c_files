/*//////////////////////////////////////////////////////////////////
//                                                                //
//                            Project A                           //
//                                                                //
//      Project A submission for subject:                         //
//        COMP20005 Engineering Computation                       //
//                                                                //
//  Created by Sande Harsa on 8/30/12.                            //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <math.h>

#define PI 3.141592653589793
#define X_AXIS_LEN 35
#define Y_AXIS_LEN 30
#define TICKS_INVAL 5

double flow_rate(double, double, double);
void grapher(double[]);

int
main(int argc, char **argv) {
    double diameter, manning_coefficient, slope, flow, header=0;
    double min_slope, min_mann, mean_mann;
    int info_dia=0, data_count=0;
    
    while(scanf("%lf,%lf,%lf", &diameter, &manning_coefficient, &slope) == 3) {
        /* Stage 1 */
        if(header == 0) {
            printf("Stage 1\n\n");
            printf("Diameter  Manning  Slope  Flow Rate\n");
            printf("========  =======  =====  =========\n");
            min_slope = slope;
            min_mann = manning_coefficient;
            header = 1;
        }
        flow = flow_rate(diameter,manning_coefficient,slope);
        
        if (data_count < 3) {
            printf("%.4f %9.4f %8.4f %.4e\n", diameter, manning_coefficient,
                   slope, flow);
        }
        
        /* Stage 2 Calculations */
        if (diameter > 0.1) {
            info_dia += 1;
        }
        if (slope < min_slope) {
            min_slope = slope;
            min_mann = manning_coefficient;
        }
        mean_mann += manning_coefficient;
        data_count += 1;
        
    }
    mean_mann = mean_mann/data_count;
    printf("\n");
    
    /* Stage 2 Printing */
    printf("Stage 2\n\n");
    printf("Number of pipes with diameter > 0.1m = %d\n", info_dia);
    printf("Minimum slope = %.4f with Manning coefficient = %.4f\n", min_slope,
           min_mann);
    printf("Average Manning coefficient = %.4f\n", mean_mann);
    printf("\n");
    
    /* Stage 3 - Calculating Flow Rates */
    double flow_rates[6];
    double i;
    int j;
    for (i=0.25,j=0;i<=0.5;i+=0.05,j++) {
        flow_rates[j] = flow_rate(i,mean_mann,min_slope);
    }
    
    /* Stage 3 Printing */
    printf("Stage 3\n\n");
    printf("Flow rate (x 10^-3) m^3/s\n");
    grapher(flow_rates);
    printf("      Diameter (x 10^-2) m\n");
    
    return 0;
}

double
flow_rate(double diameter, double manning_coefficient, double slope) {
    double area, radius_h, wp, Q;
    wp = (PI*diameter)/2;          /* Wetted perimeter */
    area = (PI*pow(diameter,2))/8; /* Crossectional area */
    radius_h = area/wp;            /* Hydraulic radius */
    
    Q = (1/manning_coefficient)*pow(radius_h,2.0/3.0)*area*pow(slope,0.5);
    
    return Q;
}

void
grapher(double frate[]) {
    int xaxis,yaxis,star_pos=30,space,index_rate=5, xticks_n;
    double rate_graph, compare;
    
    /* Printing the y-axis */
    for (yaxis=Y_AXIS_LEN;yaxis>=0;yaxis--) {
        rate_graph = yaxis;
        if(yaxis != 0 && yaxis%5 == 0) {
            printf("%d", yaxis);
        } else {
            printf("|");
        }
        
        /* Round up flow rate to three decimals for comparison */
        compare = (int)(frate[index_rate]*1000)+1;
        
        /* Printing star points */
        if((index_rate>=0) && (compare == rate_graph)) {
            for(space=0;space<star_pos-1;space++) {
                printf(" ");
            }
            printf("*");
            star_pos -= 5;
            index_rate--;
        }
        if(yaxis != 0) {
            printf("\n");
    
        }
    }
    
    /* Printing the x-axis */
    double xticks[] = {2.5,3.0,3.5,4.0,4.5,5.0};
    for (xaxis=1,xticks_n=0;xaxis<X_AXIS_LEN;xaxis++) {
        if (xaxis%TICKS_INVAL == 0) {
            printf("%d", (int)xticks[xticks_n]);
            xticks_n += 1;
        } else {
            printf("-");
        }
    }
    printf("\n ");
    for (xaxis=1;xaxis<X_AXIS_LEN;xaxis++) {
        if (xaxis%TICKS_INVAL == 0) {
            if(xaxis%2 == 0) {
                printf("0");
            } else {
                printf("5");
            }
        } else {
            printf(" ");
        }
    }
    printf("\n");
}

