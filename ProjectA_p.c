/*//////////////////////////////////////////////////////////////////
//                                                                //
//                            Project A                           //
//                                                                //
//      Project A submission for subject:                         //
//        COMP20005 Engineering Computation                       //
//                                                                //
//  Created by Sande Harsa on 8/30/12.                            //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <math.h>

#define PI (3.141592653589793)

double flow_rate(double, double, double);
void sum_info(double, double, double, int*, double*, double*, double*);

int
main(int argc, char **argv) {
    double diameter, manning_coefficient, slope, flow, header=0;
    double min_slope=0, min_mann=0, mean_mann=0;
    double *pmin_slope=&min_slope,*pmin_mann=&min_mann,*pmean_mann=&mean_mann;
    int info_dia=0, data_count=0;
    int *pinfo_dia=&info_dia;
    
    while(scanf("%lf,%lf,%lf", &diameter, &manning_coefficient, &slope) == 3) {
        if(header == 0) {
            printf("Stage 1\n\n");
            printf("Diameter  Manning  Slope  Flow Rate\n");
            printf("========  =======  =====  =========\n");
            *pmin_slope = slope;
            *pmin_mann = manning_coefficient;
        }
        flow = flow_rate(diameter,manning_coefficient,slope);
        printf("%.4f %9.4f %8.4f %.4e\n", diameter, manning_coefficient,
               slope, flow);
        header = 1;
        /* Stage 2 call */
        sum_info(diameter, manning_coefficient, slope, pinfo_dia, pmean_mann,
                 pmin_mann, pmin_slope);
        data_count += 1;
    
    }
    /* Stage 2 Printing */
    printf("Stage 2\n\n");
    printf("Number of pipes with diameter > 0.1m = %d\n", info_dia);
    printf("Minimum slope = %.4f with Manning coefficient = %.4f\n", min_slope,
           min_mann);
    printf("Average Manning coefficient = %.4f\n", mean_mann/data_count);
    
    /* Stage 3 Printing */
    printf("Stage 3\n\n");
    
    return 0;
}

double
flow_rate(double diameter, double manning_coefficient, double slope) {
    double area, radius_h, wp, Q;
    wp = (PI*diameter)/2;          /* Wetted perimeter */
    area = (PI*pow(diameter,2))/8; /* Crossectional area */
    radius_h = area/wp;            /* Hydraulic radius */
    
    Q = (1/manning_coefficient)*pow(radius_h,2.0/3.0)*area*pow(slope,0.5);
    return Q;
}

void
sum_info(double d, double m, double s, int *info_dia, double *mean_mann,
         double *min_mann, double *min_slope) {
    if (d > 0.1) {
        *info_dia += 1;
    }
    if (s < *min_slope) {
        *min_slope = s;
        *min_mann = m;
    }
    *mean_mann += m;
}

