/*
//  ProjectB-v2.c
//  alternate version of functions for ProjectB
//
//  Created by Sande Harsa on 10/10/12.
//
*/

#include <stdio.h>

/* Construct a land_t of a local neighbourhood (square area)
 relative to a point. Return the size of it. */
point_t
find_neighbours(land_t *landscape, land_t *nhood, point_t p) {
    int x0, y0, nx=0, ny=0, check;
    point_t size;
    cell_t site;
    site.risk = 0, site.status = 0;
    /* Iterate steps needed from the local site to construct
     a square neighbourhood */
    for(y0=NLOCAL_MIN;y0<=NLOCAL_MAX;y0++) {
        for(x0=NLOCAL_MIN;x0<=NLOCAL_MAX;x0++) {
            check = 0;
            nx += 1;
            ny += 1;
            /* Don't copy over the value of the site itself */
            if (x0==0 && y0==0) {
                (*nhood)[x0+1][y0+1] = site;
            } else if((check=point_check(p.x+x0,p.y+y0))==0) {
                (*nhood)[x0+1][y0+1] = (*landscape)[p.x+x0][p.y+y0];
            }
            else if(check==1) nx -= 1;
            else if(check==-1) ny -= 1;
        }
    }
    /* Calculate the x*y size of the neighbourhood */
    /* necessary since at endpoints, the neighbourhood could be in different
     size */
    size.x = nx/NLOCAL_X;
    size.y = ny/NLOCAL_Y;
    return size;
}

point_t
find_neighbours(point_t neighbourhood[], int size) {
    int x0, y0, x, y, nx=0, ny=0, check;
    point_t size;
    cell_t site;
    site.risk = 0, site.status = 0;
    /* Iterate steps needed from the local site to construct
     a square neighbourhood */
    for(y=0, y0=NLOCAL_MIN;y<NLOCAL_Y && y0<=NLOCAL_MAX;y0++,y++) {
        for(x=0, x0=NLOCAL_MIN;x<NLOCAL_X && x0<=NLOCAL_MAX;x0++) {
            check = 0;
            nx += 1;
            ny += 1;
            /* Don't copy over the value of the site itself */
            if (x==1 && y==1) {
                (*nhood)[x0+1][y0+1] = site;
            } else if((check=point_check(p.x+x0,p.y+y0))==0) {
                (*nhood)[x][y] = (*landscape)[p.x+x0][p.y+y0];
                x++;
            }
            else if(check==1) nx -= 1;
            else if(check==-1) ny -= 1;
        }
    }
    /* Calculate the x*y size of the neighbourhood */
    /* necessary since at endpoints, the neighbourhood could be in different
     size */
    size.x = nx/NLOCAL_X;
    size.y = ny/NLOCAL_Y;
    return size;
}