 /*//////////////////////////////////////////////////////////////////
//                                                                //
//                            Project B                           //
//                                                                //
//      Project B submission for subject:                         //
//        COMP20005 Engineering Computation                       //
//                                                                //
//  Created by Sande Harsa on 9/10/12.                            //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Global variables */
#define BURNT 1         /* site status */
#define NOT_BURNT 0     /* site status */
#define SIZE_X 40       /* size of land */
#define SIZE_Y 40       /* size of land */
#define SITE_LOC_X 20   /* site location */
#define SITE_LOC_Y 20   /* site location */
#define MAX_INPUT 10    /* maximum input (coordinates) */
#define RISK_TRESH 0.8  /* treshold on 'high risk' sites */
#define NLOCAL 9        /* square area of the local neighbourhood */
#define NSIMS 10        /* number of loops through the fire simulation */
#define NSEED 500       /* number of random seeds for monte carlo */
#define SEED_MAX 4000   /* maximum value of seeds */
#define CELLTOKM2 5*5   /* Convert 'cell' to km^2 */

/* Typedef(s) */
typedef struct {
    int status;
    double risk;
} cell_t;

typedef struct {
    int x;
    int y;
} point_t;

typedef cell_t land_t[SIZE_X][SIZE_Y];

/* Function prototypes */
double calculate_area_burnt(land_t*);
double calculate_risk(land_t*, int, int);
double monte_carlo_simulation(int*);
int calculate_distance(int, int, int, int);
int find_neighbours(point_t*, point_t);
int get_coordinates(point_t[], int);
int point_check(int, int);
int risk_check(cell_t);
land_t *make_landscape(int);
point_t random_point();
void fire_spread_simulation(land_t*, point_t, int);
void print_land_map(land_t*, point_t);
void seed_generator(int*);

int
main(int argc, char **argv) {
    int i, j, d, npoints, n_risk, nloc;
    int seed, x1 = SITE_LOC_X, y1 = SITE_LOC_Y;
    int *randps;
    double risk_proportion, average_area_burnt;
    land_t *landscape;
    point_t coors[MAX_INPUT], *local, randp;
    local = malloc(NLOCAL*sizeof(point_t));
    randps = malloc(NSEED*sizeof(int));
    /* read command line arguments */
    if (argc == 2) {
        seed = atoi(argv[1]);
        landscape = make_landscape(seed);
    } else {
        printf("Usage: ./ProjectB <seed>\n");
        exit(EXIT_FAILURE);
    }
    /* Reading input data */
    npoints = get_coordinates(coors, MAX_INPUT);
    /* Stage 1 - Processing */
    risk_proportion = calculate_risk(landscape, SIZE_X, SIZE_Y)/(SIZE_X*SIZE_Y);
    /* Stage 1 - Printing */
    printf("Stage 1\n");
    printf("=======\n\n");
    printf("Proportion of cells with a high risk fire rating = %.3f\n\n",
           risk_proportion);
    /* Stage 2 - Process & Print */
    printf("Stage 2\n");
    printf("=======\n\n");
    for (i=0;i<npoints;i++) {
        d = calculate_distance(x1, y1, coors[i].x, coors[i].y);
        printf("Distance from (%2d,%2d) to (%2d,%2d) = %2d\n",
               x1, y1, coors[i].x, coors[i].y, d);
    }
    printf("\n");
    /* Stage 3 - Process & Print */
    printf("Stage 3\n");
    printf("=======\n\n");
    /* iterate through input points and each neighbour points */
    for (i=0;i<npoints;i++) {
        n_risk = 0;
        nloc = find_neighbours(local, coors[i]);
        for(j=0;j<nloc;j++) {
            if(risk_check((*landscape)[local[j].x][local[j].y])) {
                n_risk++;
            }
        }
        printf("Number of neighbour cells for site ");
        printf("(%2d,%2d) with a high risk rating = %d\n",
               coors[i].x, coors[i].y, n_risk);
    }
    printf("\n");
    /* Stage 4 - Processing */
    randp = random_point();
    fire_spread_simulation(landscape, randp, NSIMS);
    /* Stage 4 - Print */
    printf("Stage 4\n");
    printf("=======\n\n");
    print_land_map(landscape, randp);
    printf("\n");
    /* Stage 5 - Process */
    seed_generator(randps);
    average_area_burnt = monte_carlo_simulation(randps);
    average_area_burnt = average_area_burnt * CELLTOKM2; /* convert to km^2 */
    /* Stage 5 - Printing */
    printf("Stage 5\n");
    printf("=======\n\n");
    printf("The expected area burnt is %.3f km^2\n", average_area_burnt);
    return 0;
}

/* Construct a landscape with an argument seed to randomize cells' contents */
land_t
*make_landscape(int seed) {
    /* Variables initialization */
    int x, y;
    land_t *landscape;
    landscape = (land_t*)malloc(sizeof(*landscape));
    srand(seed);
    /* Looping through cells (fill up rows) */
    for(x=0;x<SIZE_X;x++) {
        for(y=0;y<SIZE_Y;y++) {
            (*landscape)[x][y].status = NOT_BURNT;
            (*landscape)[x][y].risk = rand()/(1.0+RAND_MAX);
        }
    }
    return landscape;
}

/* Read coordinates data, return the number of points read */
int
get_coordinates(point_t coors[], int limit) {
    int x, y, count=0;
    while(scanf("%d, %d", &x, &y)==2) {
        if(count == limit) {
            break;
        }
        coors[count].x = x;
        coors[count].y = y;
        count++;
    }
    return count;
}

/* Construct a land_t of a local neighbourhood (square area)
 relative to a point. Return the size of it. */
int
find_neighbours(point_t *neighbourhood, point_t p) {
    int limit, i, j, count=0;
    /* offset from the central point */
    int offsetx[]={-1,0,1}, offsety[]={-1,0,1};
    /* set limit on how iterate around the central point */
    limit = (int)sqrt(NLOCAL);
    for(i=0;i<limit;i++) {
        for(j=0;j<limit;j++) {
            if((i==1) && (j==1)) continue;
            if(!point_check(p.x-offsetx[i], p.y-offsety[j])) {
                neighbourhood[count].x = p.x-offsetx[i];
                neighbourhood[count].y = p.y-offsety[j];
                count++;
            }
        }
    }
    return count;
}

/* Simulate fire spreading recursively */
void
fire_spread_simulation(land_t *landscape, point_t p, int t) {
    int i, nloc;
    double current_risk, source_risk;
    point_t local[NLOCAL];
    /* set the argument cell to burnt */
    (*landscape)[p.x][p.y].status = BURNT;
    /* recursion - base case */
    if(t<=0) {
        return;
    }
    /* recursive part */
    nloc = find_neighbours(local, p);
    for(i=0;i<nloc;i++) {
        current_risk = (*landscape)[local[i].x][local[i].y].risk;
        source_risk = (*landscape)[p.x][p.y].risk;
        if(source_risk<=current_risk) {
            fire_spread_simulation(landscape,local[i],t-1);
        }
    }
}

/* Monte Carlo simulations (consist of repeated fire spreading simulation) */
double
monte_carlo_simulation(int *randps) {
    int i, seed;
    double total_area_burnt=0, average_area_burnt;
    land_t *current;
    point_t start_point;
    for(i=0;i<NSEED;i++) {
        /* variables */
        srand(seed=randps[i]);
        current = make_landscape(seed);
        start_point = random_point();
        /* the process */
        fire_spread_simulation(current, start_point, NSIMS);
        total_area_burnt += calculate_area_burnt(current);
        /* free malloc'ed space each end of the loop */
        free(current);
        current = NULL;
    }
    average_area_burnt = total_area_burnt/NSEED;
    return average_area_burnt;
}

/* Checks whether a point is valid or not */
int
point_check(int x, int y) {
    if (x>=SIZE_X || x<0) {
        return 1;
    } else if (y>=SIZE_Y || y<0) {
        return -1;
    } else {
        return 0;
    }
}

/* Checks whether a point is in high risk or not */
int
risk_check(cell_t p) {
    if(p.risk > RISK_TRESH) {
       return 1;
    }
    return 0;
}

/* Pick a random point from the landscape */
point_t
random_point() {
    point_t p;
    p.x = rand() % SIZE_X;
    p.y = rand() % SIZE_Y;
    return p;
}

/* Generate random ints (seeds) and put it into the argument array */
void
seed_generator(int *A) {
    int i;
    for(i=0;i<NSEED;i++) {
        A[i] = rand()%SEED_MAX;
    }
}

/* Calculate the sum of high risk sites(points/cells) in a landscape */
double
calculate_risk(land_t *landscape, int size_x, int size_y) {
    int x, y;
    double high_risk = 0;
    for(x=0;x<size_x;x++) {
        for(y=0;y<size_y;y++) {
            if(risk_check((*landscape)[x][y])) {
                high_risk += 1;
            }
        }
    }
    return high_risk;
}

/* Calculate the Manhattan  distance between two points */
int
calculate_distance(int x1, int y1, int x2, int y2) {
    int d;
    d = abs(x2-x1) + abs(y2-y1);
    return d;
}

/* Calculate burnt area on a given landscape */
double
calculate_area_burnt(land_t *landscape) {
    int x, y;
    double nburnt=0;
    for(x=0;x<SIZE_X;x++)
        for(y=0;y<SIZE_Y;y++)
            if((*landscape)[x][y].status==BURNT)
                nburnt++;
    return nburnt;
}

/* Print out a land_t as a 2D map (including details) */
void
print_land_map(land_t *landscape, point_t p) {
    int x, y, nburnt=0, max_dist=0, dist;
    printf("A map of the fire spread after %d time steps for a fire source\n",
           NSIMS);
    printf("located at (%d,%d). # represents a burnt cell.\n\n", p.x, p.y);
    for(x=0;x<SIZE_X;x++) {
        for(y=0;y<SIZE_Y;y++) {
            if((*landscape)[x][y].status==NOT_BURNT) {
                printf(". ");
            } else {
                printf("# ");
                nburnt++;
                dist = calculate_distance(p.x,p.y,x,y);
                if(dist>max_dist) {
                    max_dist = dist;
                }
            }
        }
        printf("\n");
    }
    printf("\n");
    printf("Total number of sites (or cells) burnt = %d\n", nburnt);
    printf("Maximum distance = %d\n", max_dist);
}
