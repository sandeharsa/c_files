/*
  amicable.c
  

  Created by Sande Harsa on 8/6/12.

*/

#include <stdio.h>
#include <stdlib.h>

#define IS_AMI 1
#define NOT_AMI 0

int sumfac(int);
int amicable(int, int);

int
main(int argc, char **argv) {
    int x, y, num, i, j, cur=0;
    printf("Enter two numbers:\n");
    if (scanf("%d%d", &x, &y) != 2) {
        printf("Invalid input. ");
        printf("Please enter two numbers.\n");
        exit(EXIT_FAILURE);
    }
    if(amicable(x,y)) {
        printf("The pair is amicable.\n");
    } else {
        printf("The pair is NOT amicable.\n");
    }
    
    printf("-----------------------------\n");
    printf("Enter the number of pairs you want to search:\n");
    scanf("%d", &num);
    /* Search for amicable pairs */
    j=0;
    for(i=1;j!=num;i++) {
        /* i != cur so that there are no repeats */
        if ((sumfac(i) > i) && (i != cur) && (amicable(sumfac(i),i))) {
            cur = sumfac(i);
            printf("%d. %3d %3d\n", j+1, i, sumfac(i));
            j++;
        }
    }
    return 0;
}

int
sumfac(int num) {
    int sum=1, divisor;
    for(divisor = 2; divisor*divisor <= num; divisor++) {
        if (num%divisor == 0) {
            sum += divisor;
            sum += num/divisor;
        }
    }
    return sum;
}

int
amicable(int x, int y) {
    if(sumfac(x) == y && sumfac(y) == x && x != y) {
        return IS_AMI;
    } else {
        return NOT_AMI;
    }
}



/* 220 284 */