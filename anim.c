/*
//  anim.c
//  
//
//  Created by Sande Harsa on 10/2/12.
//
*/

#include <stdio.h>

int
main(int argc, char **argv) {
    int i, j;
    printf("  o\n");
    printf(" /|\\\n");
    printf("  √\\o");
    for (i=0;i<10;i++) {
        printf("\b");
        for(j=0;j<=i;j++) {
            printf(" ");
        }
        printf("o");
    }
    
    return 0;
}