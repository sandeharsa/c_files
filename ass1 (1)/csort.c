/*
** Use counting sort to sort A in increasing order.
** Return pointer to the resulting array.
**
** Author: Sande Harsa
** Date: 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "csort.h"

/* Find maximum value from an array A, size n*/
int
max(unsigned int *A, int n) {
	int i, m;
	m = A[0];
	for(i=1;i<n;i++)
		if(A[i] > m) m = A[i];
	return m;
}

unsigned int *
countingSort(unsigned int *A, int n) {
	int i, j, m;
	unsigned int *C;
	/* Initialize Array C[] */
	m = max(A, n);
	C = (int *)malloc(sizeof(uint) * (m+1));
	memset(C,0,sizeof(uint) * (m+1));
	for (i=0;i<m;i++) C[i] = 0;
	/* Count values */
	for(i=0;i<n;i++)
		C[A[i]] += 1;
	/* Read counted values from C[] to make a sorted array */
	i=0, j=0;
	while(i<n && j<=m) {
		if(C[j] > 0) {
			A[i] = j;
			C[j] -= 1; /* remove counter */
			i++; 
		} else j++;
	}
	/* for(i=0;i<n;i++) printf("%d ", A[i]);
	printf("\n"); */
    return A;
}
