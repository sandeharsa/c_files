/* 
 COMP20007 Design of Algorithms 

 Read in a text file of integers (one per line) from stdin and call either 
 Counting Sort or MSB Radix Sort depending on command line params.

 Author: Andrew Turpin (aturpin@unimelb.edu.au)
 Date: March 2013

 Usage: ass1Sort [c|r] < filename

 Modified Fri 15 Mar 2013 : only allows c or r as arguments
                          : skips junk lines of input
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "type.h"
#include "csort.h"
#include "rsort.h"

#define MAX_INTEGER_STRING_LEN 64  /* maximum length in chars of integer */

/*
** Print usage message
*/
void
usage(char *exeName) { 
    fprintf(stderr,"Usage: %s [c | r] < filename\n",exeName);
    fprintf(stderr,"       where\n");
    fprintf(stderr,"          c - counting sort\n");
    fprintf(stderr,"          r - msb radix sort\n");
    fprintf(stderr,"          filename - file name of text file of integers, one per line\n");
}

/*
** Check A[0..n-1] is sorted in increasing order
*/
void 
checkSorted(uint *A, int n) {
    printf("Checking if sorted...");

    char sorted = 1;

    if (A == NULL && n > 0)
        sorted = 0;

    int i;
    for(i = 1 ; i < n  && sorted ; i++)
        sorted = A[i-1] <= A[i];

    if (sorted)
        printf("SORTED: nice!\n");
    else
        printf("UNSORTED!!!\n");
    return;
}

/*
** Read input, call sort, print time, check sorted
*/
int 
main(int argc, char *argv[]) {

    if (argc != 2) {
        usage(argv[0]);
        return(-1);
    }

    /*
    ** Read in the integers, and keep increasing the array size
    ** if necessary.
    */
    int maxArraySize = 64;  /* any number > 0 */
    int n  = 0;
    uint *A = (int *)malloc(sizeof(uint) * maxArraySize);
    if (A == NULL) {
        fprintf(stderr, "Out of memory for A\n");
        return -1;
    }
    char buff[MAX_INTEGER_STRING_LEN];
    while (fgets(buff, MAX_INTEGER_STRING_LEN, stdin) != NULL) {
        if (n >= maxArraySize) {
            maxArraySize *= 2;  /* double the size of the array */
            A = (int *)realloc(A, sizeof(int) * maxArraySize);
            if (A == NULL) {
                fprintf(stderr, "Out of memory for A\n");
                return -1;
            }
        }
        if (sscanf(buff, "%d", &A[n]) == 1)
            n++;
    }
    
    printf("Read %d numbers\n",n);

    uint *B = NULL;
    if (argv[1][0] == 'c') {
        clock_t startTime = clock();
        B = countingSort(A, n);
        clock_t endTime = clock();
        printf("Time taken to countingSort %d elements = %ldms\n", n, (endTime-startTime+500)/1000);
    } else if (argv[1][0] == 'r') {
        clock_t startTime = clock();
        B = radixSort(A, n);
        clock_t endTime = clock();
        printf("Time taken to radixSort %d elements = %ldms\n", n, (endTime-startTime+500)/1000);
    } else 
        usage(argv[0]);
    
    checkSorted(B, n);
    
    return 0;
}
