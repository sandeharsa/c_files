/*
** Use MSB radix sort to sort A in increasing order.
** Return pointer to the resulting array.
**
** Author: Sande Harsa
** Date: 
*/

#include <stdlib.h>
#include <stdio.h>
#include "rsort.h"

#define BITS 32

/*
** Return the k'th bit of i. (Bit 1 is the least significant bit)
*/
int getBit(int i, int k) {
    return (i >> (k-1)) & 0x01;
}

/* Radix Sort */
unsigned int *
radixSort(unsigned int *A, int n) {
	int i;
	A = radixSortRec(A,n,BITS);
    return A;
}

/* Swap values between int pointers */
void
swapint(unsigned int *x, unsigned int *y) {
	uint t;
	t = *x;
	*x = *y;
	*y = t;
}

/* Recursive Radix Sort */
unsigned int *
radixSortRec(unsigned int *A, int n, int k) {
	int i=n-1, o=0; 
	/* Base Case */
	if(k==0) 
		return A;
	/* Recursive Case */
	while(o<i) {
		/* Split array depend on k-th bit (0 or 1) */
		if(getBit(A[o], k)) {
			swapint(&(A[o]),&(A[i]));
			i--;
		} else {
			o++;
		}
	}
	
	/* for(i=0;i<n;i++) printf("%d ", A[i]);
	printf("\n"); */
	
	return radixSortRec(A,o,k-1);
}
