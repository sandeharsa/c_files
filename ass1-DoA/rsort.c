/*
** Use MSB radix sort to sort A in increasing order.
** Return pointer to the resulting array.
**
** Author: Sande Harsa
** Date: 25 March 2013
*/

#include <stdlib.h>
#include "rsort.h"
#include "type.h"

#define BITS 32

/*
** Return the k'th bit of i. (Bit 1 is the least significant bit)
*/
int getBit(int i, int k) {
    return (i >> (k-1)) & 0x01;
}

/* Radix Sort */
unsigned int *
radixSort(unsigned int *A, int n) {
	return radixSortRec(A,n,BITS);
}

/* Swap values between int pointers */
void
swapint(unsigned int *x, unsigned int *y) {
	uint t;
	t = *x;
	*x = *y;
	*y = t;
}

/* Recursive Radix Sort */
unsigned int *
radixSortRec(unsigned int *A, int n, int k) {
	int i=n-1, o=0;
	/* Base Case */
	if(k==0 | n<=1)
		return A;
	while(o<=i) {
		/* Split array depend on k-th bit (0 or 1) */
		if(getBit(A[o], k)) {
            swapint(&(A[o]),&(A[i]));
			i--;
		} else {
			o++;
		}
	}
	/* Recursive Calls */
	radixSortRec(A,o,k-1);
	radixSortRec(A+o,n-o,k-1);
	return A;
}
