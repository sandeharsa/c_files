/*
** Use MSB radix sort to sort A in increasing order.
** Return pointer to the resulting array.
**
** Author: Sande Harsa
** Date: 
*/

#include <stdlib.h>
#include <stdio.h>
#include "rsort.h"

#define BITS 32

/*
** Return the k'th bit of i. (Bit 1 is the least significant bit)
*/
int getBit(int i, int k) {
    return (i >> (k-1)) & 0x01;
}

/* Radix Sort */
unsigned int *
radixSort(unsigned int *A, int n) {
	A = radixSortRec(A,n,BITS);
    return A;
}

/* Recursive Radix Sort */
unsigned int *
radixSortRec(unsigned int *A, int n, int k) {
	int x, y, p;
	int i=0, o=0; 
	uint *Ao, *Ai;
	/* Initialize temp arrays */
	Ao = (uint *)malloc(sizeof(uint) * n);
	Ai = (uint *)malloc(sizeof(uint) * n);
	/* Base Case */
	if(k==0 || n==0) 
		return A;
	/* Recursive Case */
	for(x=0;x<n;x++) {
		/* Split array depend on k-th bit (0 or 1) */
		if(getBit(A[x], k)) {
			Ai[i] = A[x];
			i++;
		} else {
			Ao[o] = A[x];
			o++;
		}
	}
	/* Recursive calls */
	Ao = radixSortRec(Ao,o,k-1);
	Ai = radixSortRec(Ai,i,k-1);
	/* Insert Ao into A and then Ai into A */
	for(x=0;x<o;x++) A[x] = Ao[x];
	for(y=0;y<i && x<n;y++,x++) A[x] = Ai[y];
	
	return A;
}
