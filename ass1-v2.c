//
//  ass1-v2.c
//  
//
//  Created by Sande Harsa on 9/18/12.
//
//

#include <stdio.h>

typedef struct {
    int plus_start; /* =1, word will effect score when in starting pos */
    int plus_end; /* =1, word will effect score when in ending pos */
    int que_i; /* >0, the word = queries[que_i] */
} wscore_t;

/* Generate the maximum score of possible snippets from a paragraph */
void
snippet_scoring(para_t *p, int n_que, char **queries) {
    int i, j, k, q, len_w;
    float cur_scr=0;
    char punc;
    int *word_found;
    int *punc_found;
    /* array initializations */
    if ((word_found = malloc((n_que-1)*sizeof(int))) == NULL) {
        printf("Failed to allocate memory - word_found\n");
        exit(EXIT_FAILURE);
    }
    if ((punc_found = malloc((p->nwrds)*sizeof(int))) == NULL) {
        printf("Failed to allocate memory - punc_found\n");
        exit(EXIT_FAILURE);
    }
    memset(punc_found, 0, (p->nwrds)*sizeof(int));
    p->max_snippet_scr.score = 0;
    /* iterate over possible starting words for the snippets */
    for (i=0;i<=(p->nwrds-MIN_SNIPPET_LEN);i++) {
        /* iterate over 10 possible words (for each start word)
         for the end word */
        for (j=(i+MIN_SNIPPET_LEN);j<=(i+MAX_SNIPPET_LEN) && j<=p->nwrds;j++) {
            cur_scr = 0;
            memset(word_found, 0, (n_que-1)*sizeof(int));
            /* add 0.9 if the snippets start with the first word */
            if (i==0) {
                cur_scr += 0.9;
            }
            if (i>0 && punc_found[i-1]) {
                cur_scr += 0.9;
            }
            /* iterate over the snippet itself */
            for (k=i;k<j;k++) {
                /* remove any punctuations before checking */
                punc = '\0';
                len_w = strlen(p->words[k]);
                if (strchr(TERM_PUNCT, p->words[k][len_w-1])) {
                    punc = p->words[k][len_w-1];
                    p->words[k][len_w-1] = '\0';
                    len_w--;
                    /* note the position of the punctuated word */
                    punc_found[k] = 1;
                }
                /* run through queries and check */
                for (q=1;q<n_que;q++) {
                    if (strlen(queries[q])==len_w &&
                        !strncasecmp(p->words[k],queries[q],len_w)) {
                        /* add 2.4 when a new word is found,
                         add 1.0 if it is a repeated word */
                        if (!word_found[(q-1)]) {
                            cur_scr += 2.4;
                            word_found[q-1] = 1;
                        } else if (word_found[(q-1)]){
                            cur_scr += 1.0;
                        }
                        break;
                    }
                }
                /* put the punctuation back in */
                if (punc) {
                    p->words[k][len_w] = punc;
                }
            }
            /* add 0.7 if the last word is punctuated */
            if (punc) {
                cur_scr += 0.7;
            }
            /* compare snippet scores */
            if (cur_scr>(p->max_snippet_scr.score)) {
                p->max_snippet_scr.score = cur_scr;
                p->max_snippet_scr.start_i = i;
                p->max_snippet_scr.end_i = k;
            }
        }
    }
    /* free allocated memory */
    free(word_found);
    word_found = NULL;
    free(punc_found);
    punc_found = NULL;
}

void
snippet_scoring1(para_t *p, int n_que, char **queries) {
    int i, q, start, base_start=0, len_w;
    char punc;
    float cur_scr=0, iter_scr=0;
    int *word_found;
    wscore_t *p_score;
    /* array initializations */
    if ((word_found = malloc((n_que-1)*sizeof(int))) == NULL) {
        printf("Failed to allocate memory - word_found\n");
        exit(EXIT_FAILURE);
    }
    memset(word_found, 0, (n_que-1)*sizeof(int));
    if ((p_score = malloc((p->nwrds)*sizeof(wscore_t))) == NULL) {
        printf("Failed to allocate memory - w\n");
        exit(EXIT_FAILURE);
    }
    for (i=0;i<p->nwrds;i++) {
        p_score[i].plus_start = 0;
        p_score[i].plus_end = 0;
        p_score[i].que_i = -1;
    }
    /* Flag to modify score:
     Add 0.9 when the snippet start with the paragraph starting word */
    p_score[0].plus_start = 1;
    
    p->max_snippet_scr.score = 0;
    /* iterate over paragraph words */
    for (i=0;i<(p->nwrds);i++) {
        /* remove any punctuations before checking */
        punc = '\0';
        len_w = strlen(p->words[i]);
        if (strchr(TERM_PUNCT, p->words[i][len_w-1])) {
            punc = p->words[i][len_w-1];
            p->words[i][len_w-1] = '\0';
            len_w--;
            /* note the position of the punctuated word */
            if ((i+1)<(p->nwrds)) {
                p_score[i+1].plus_start = 1;
            }
            p_score[i].plus_end = 1;
        }
        /* run through queries and check */
        for (q=1;q<n_que;q++) {
            if (strlen(queries[q])==len_w &&
                !strncasecmp(p->words[i],queries[q],len_w)) {
                /* add 2.4 when a new word is found,
                 add 1.0 if it is a repeated word */
                if (!word_found[(q-1)]) {
                    cur_scr += 2.4;
                    word_found[q-1] = 1;
                    p_score[i].que_i = (q-1);
                } else if (word_found[(q-1)]){
                    cur_scr += 1.0;
                    word_found[q-1] += 1;
                    p_score[i].que_i = (q-1);
                }
                break;
            }
        }
        /* put the punctuation back in */
        if (punc) {
            p->words[i][len_w] = punc;
        }
        /* initialize base starting point */
        if ((i-(MAX_SNIPPET_LEN+base_start)==0)) {
            base_start++;
            if (p_score[base_start-1].que_i>=0) {
                if (word_found[p_score[base_start-1].que_i]>1) {
                    cur_scr -= 1.0;
                    word_found[p_score[base_start-1].que_i] -= 1;
                } else if (word_found[p_score[base_start-1].que_i]==1){
                    cur_scr -= 2.4;
                    word_found[p_score[base_start-1].que_i] -= 1;
                }
            }
        }
        /* Add the modifier from the current ending word */
        if (p_score[i].plus_end) {
            cur_scr += 0.7;
        }
        /* Deduct the modifier from the previous ending word */
        if ((i-1>=0) && p_score[i-1].plus_end) {
            cur_scr -= 0.7;
        }
        iter_scr = cur_scr;
        start = base_start;
        /* iterate over possible starting words for the current end word (i) */
        while(start>=0 && (i-start>=(MIN_SNIPPET_LEN-1)) &&
              (i-start<MAX_SNIPPET_LEN)) {
            /* Add the modifier from the current starting word */
            iter_scr += p_score[start].plus_start*0.9;
            /* Deduct the modifier from the last starting word */
            if (start-1>=0) {
                iter_scr -= p_score[start-1].plus_start*0.9;
                if (p_score[start-1].que_i>0) {
                    if (word_found[p_score[start-1].que_i]>1) {
                        iter_scr -= 1.0;
                    } else if (word_found[p_score[base_start-1].que_i]==1){
                        iter_scr -= 2.4;
                    }
                }
            }
            if (iter_scr>(p->max_snippet_scr.score)) {
                p->max_snippet_scr.score = iter_scr;
                p->max_snippet_scr.start_i = start;
                p->max_snippet_scr.end_i = i+1;
            }
            start++;
        }
    }
    /* free allocated memory */
    free(word_found);
    word_found = NULL;
    free(p_score);
    p_score = NULL;
}