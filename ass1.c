/*//////////////////////////////////////////////////////////////////
//                                                                //
//                        Assignment 1                            //
//                                                                //
//     Assignment submission for subject:                         //
//        COMP10002 Foundation of Algorithms                      //
//                                                                //
//  Created by Sande Harsa on 09/10/12.                           //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>

/* constants and fixed strings */

/* HTML formatting strings */
#define TOP "<html><head><title>Algorithms are Fun!</title></head>" \
"<body><h3>Algorithms are Fun!</h3><ol>"
#define PAR "<p><li>"
#define BOT "</ol></body></html>"

/* maximum number of characters per word */
#define MAX_WORD_LEN 23
/* maximum number of words per paragraph */
#define MAX_PARA_LEN 10000

/* return code from get_word if end of paragraph found */
#define PARA_END 1
/* return code from get_word if a word successfully found */
#define WORD_FND 2

/* terminating punctuation that may follow a word */
#define TERM_PUNCT ".,;:!?"
/* the string the separates paragraphs */
#define PARA_SEPARATOR "\n========\n"

/* maximum words in any output snippet */
#define MAX_SNIPPET_LEN 30
/* minimum words in any output snippet */
#define MIN_SNIPPET_LEN 20

/* maximum terms allowed on command line */
#define MAX_TERMS 50

/* return code from get_paragraph if paragraph found */
#define PARA_FND 1

/* addition to score according to rule R0-R3 */
#define R0 2.4
#define R1 1.0
#define R2 0.9
#define R3 0.7

/* type definitions */

typedef char word_t[MAX_WORD_LEN+1];

typedef struct {
    float score;
    int start_i; /* starting word's index */
    int end_i; /* ending word's index */
} snippet_t;

typedef struct {
    int plus_start; /* Flag; word will effect score when in starting pos */
    int plus_end; /* Flag: word will effect score when in ending pos */
    int que_i; /* >0, the word = queries[que_i] */
} wscore_t;

typedef struct {
	int nwrds;
	word_t words[MAX_PARA_LEN];
	snippet_t max_snippet_scr;
} para_t;

/* function prototypes */

void	start_output();
void	new_paragraph();
void	end_output();
int	get_word(word_t w, int limit);
int get_paragraph(para_t *p, int limit);
void    snippet_scoring(para_t *p, int, char**);
void	put_word(word_t w);
void	put_paragraph(para_t p);
void    put_highlight(para_t *p, int, char**);

/* where it all happens */

int
main(int argc, char **argv) {
    para_t onepar;
    start_output();
    if (argc==1) {
        printf("No queries entered.\n");
    } else {
        while (get_paragraph(&onepar, MAX_PARA_LEN)!=EOF) {
            new_paragraph();
            /* Check if the paragraph exist */
            if (onepar.nwrds!=0) {
                snippet_scoring(&onepar, argc, argv);
                put_highlight(&onepar, argc, argv);
            }
        }
    }
	end_output();
	return 0;
}

/* prelude output, to set up the HTML file */

void
start_output() {
	printf("%s\n", TOP);
}

/* start a new output paragraph in the HTML file */

void
new_paragraph() {
	printf("%s\n", PAR);
}

/* and close off the HTML output */

void
end_output() {
	printf("%s\n", BOT);
}

/* extract a single word out of the standard input, but not
 more than "limit" characters in total. One character of
 sensible trailing punctuation is retained.  Argument array
 w must be limit+1 characters or bigger. */
int
get_word(word_t w, int limit) {
	int c, len_w=limit;
	/* first, skip over any non alphanumerics */
	while ((c=getchar())!=EOF && !isalnum(c) && c!='\n') {
		/* do nothing more */
	}
	if (c==EOF) {
		return EOF;
	} else if (c=='\n') {
        *w = c;
        w += 1;
        limit -= 1;
        while(limit>0 && (c=getchar())!=EOF && (c=='=')) {
            /* another character to be stored */
            *w = c;
            w += 1;
            limit -= 1;
        }
        if (c=='\n') {
            *w = c;
            w += 1;
            limit -= 1;
        }
        /* close of the string for a moment */
        *w = '\0';
        /* Check whether it is a paragraph seperator or not */
        if (!strcmp((w-(len_w-limit)),PARA_SEPARATOR)) {
            return PARA_END;
        } else if (!isalnum(c)) {
            while ((c=getchar())!=EOF && !isalnum(c));
        }
        /* reset the pointer so it points the beginning of the string */
        w = w-(len_w-limit);
        limit = len_w;
    }
	/* ok, first character of next word has been found */
	*w = c;
	w += 1;
	limit -= 1;
    while (limit>0 && (c=getchar())!=EOF && isalnum(c)) {
		/* another character to be stored */
		*w = c;
		w += 1;
		limit -= 1;
	}
	/* take a look at that next character, is it trailing
     punctuation? */
	if (strchr(TERM_PUNCT, c) && (limit>0)) {
		/* yes, it is */
		*w = c;
		w += 1;
		limit -= 1;
	}
    /* now close off the string */
	*w = '\0';
    return WORD_FND;
}

/* Extract a paragraph from a text */
int
get_paragraph(para_t *p, int limit) {
    int ctrl;
    word_t oneword;
    p->nwrds = 0;
    /* Iterate over the input by get_word */
    while(p->nwrds<limit && (ctrl=get_word(oneword, MAX_WORD_LEN))!=EOF) {
        if (ctrl == PARA_END) {
            return PARA_FND;
        } else {
            strcpy(p->words[p->nwrds],oneword);
            p->nwrds += 1;
        }
    }
    return EOF;
}

/* Generate the maximum score of possible snippets from a paragraph.
 Algorithm: Iterate through the paragraph one time. Add and deduct the score 
 for each word (without ever reseting the score to 0).
 - Each word is compared to each query (once per word), modifying the score.
 - An array of struct (with 3 ints) represents each word. The array will 
 determine how the word will modify the score.
 - The ending word of each possible snippet is 'words[i]' and 'i' 
 keeps moving forward through the paragraph.
 - For each snippet that ends with word[i], a score is calculated 
 (by an iteration of possible starting words). This is done without 
 comparing each word again.
 - This algorithm supposedly have an order O(nq). Where n is the number of
 words in the paragraph and q is the number of queries.*/
void
snippet_scoring(para_t *p, int n_que, char **queries) {
    int i, q, start, base_start=0, len_w, min_s_len;
    char punc;
    float cur_scr=0, iter_scr=0;
    int *word_found;
    wscore_t *p_score;
    /* array initializations */
    if ((word_found = malloc((n_que-1)*sizeof(int))) == NULL) {
        printf("Failed to allocate memory - word_found\n");
        exit(EXIT_FAILURE);
    }
    memset(word_found, 0, (n_que-1)*sizeof(int));
    if ((p_score = malloc((p->nwrds)*sizeof(wscore_t))) == NULL) {
        printf("Failed to allocate memory - p_score\n");
        exit(EXIT_FAILURE);
    }
    for (i=0;i<p->nwrds;i++) {
        p_score[i].plus_start = 0;
        p_score[i].plus_end = 0;
        p_score[i].que_i = -1;
    }
    /* Flag to modify score:
     Add 0.9 when the snippet start with the paragraph starting word */
    p_score[0].plus_start = 1;
    /* Determine the minimal length of a snippet.
     The entire paragraph becomes the snippet if the len<MIN_SNIPPET_LEN */
    if (p->nwrds<MIN_SNIPPET_LEN) {
        min_s_len = p->nwrds;
    } else {
        min_s_len = MIN_SNIPPET_LEN;
    }
    /* iterate over paragraph words */
    p->max_snippet_scr.score = 0;
    for (i=0;i<(p->nwrds);i++) {
        /* remove any punctuations before checking */
        punc = '\0';
        len_w = strlen(p->words[i]);
        if (strchr(TERM_PUNCT, p->words[i][len_w-1])) {
            punc = p->words[i][len_w-1];
            p->words[i][len_w-1] = '\0';
            len_w--;
            /* Change the flag for the punctated word for R2 and R3 */
            if ((i+1)<(p->nwrds)) {
                p_score[i+1].plus_start = 1;
            }
            p_score[i].plus_end = 1;
        }
        /* run through queries and check */
        for (q=1;q<n_que;q++) {
            if (strlen(queries[q])==len_w &&
                !strncasecmp(p->words[i],queries[q],len_w)) {
                /* R0: add 2.4 when a new word is found,
                 R1: add 1.0 if it is a repeated word */
                if (!word_found[(q-1)]) {
                    cur_scr += R0;
                    word_found[q-1] = 1;
                    p_score[i].que_i = (q-1);
                } else if (word_found[(q-1)]){
                    cur_scr += R1;
                    word_found[q-1] += 1;
                    p_score[i].que_i = (q-1);
                }
                break;
            }
        }
        /* put the punctuation back in */
        if (punc) {
            p->words[i][len_w] = punc;
        }
        /* initialize base starting point */
        if ((i-(MAX_SNIPPET_LEN+base_start)==0)) {
            base_start++;
            if (p_score[base_start-1].que_i>=0) {
                if (word_found[p_score[base_start-1].que_i]>1) {
                    cur_scr -= R1;
                    word_found[p_score[base_start-1].que_i] -= 1;
                } else if (word_found[p_score[base_start-1].que_i]==1){
                    cur_scr -= R0;
                    word_found[p_score[base_start-1].que_i] -= 1;
                }
            }
        }
        /* Add the modifier from the current ending word */
        if (p_score[i].plus_end) {
            cur_scr += R3;
        }
        /* Deduct the modifier from the previous ending word */
        if ((i-1>=0) && p_score[i-1].plus_end) {
            cur_scr -= R3;
        }
        iter_scr = cur_scr;
        start = base_start;
        /* iterate over possible starting words for the current end word (i) */
        while(start>=0 && (i-start>=(min_s_len-1)) &&
              (i-start<MAX_SNIPPET_LEN)) {
            /* Add the modifier from the current starting word */
            iter_scr += p_score[start].plus_start*R2;
            /* Deduct the modifier from the last starting word */
            if (start-1>=0) {
                iter_scr -= p_score[start-1].plus_start*R2;
                if (p_score[start-1].que_i>=0) {
                    if (word_found[p_score[start-1].que_i]>1) {
                        iter_scr -= R1;
                    } else if (word_found[p_score[base_start-1].que_i]==1){
                        iter_scr -= R0;
                    }
                } 
            }
            if (iter_scr>(p->max_snippet_scr.score)) {
                p->max_snippet_scr.score = iter_scr;
                p->max_snippet_scr.start_i = start;
                p->max_snippet_scr.end_i = i+1;
            }
            start++;
        }
    }
    /* free allocated memory */
    free(word_found);
    word_found = NULL;
    free(p_score);
    p_score = NULL;
}

/* write out a non-bold word */
void
put_word(word_t w) {
	printf("%s\n", w);
}
                    
/* write out paragraphs */
void
put_paragraph(para_t p) {
    int i;
    /* Print out a snippet sized paragraph */
    for (i=0;i<MAX_SNIPPET_LEN && i<p.nwrds;i++) {
        put_word(p.words[i]);
    }
}

/* print out a snippet for a paragraph with bold & colored queries */
void
put_highlight(para_t *p, int n_que, char **queries) {
    int i, j, printed, len_w;
    int start, end;
    char punc;
    if (p->max_snippet_scr.start_i) {
        start = p->max_snippet_scr.start_i;
    } else {
        start = 0;
    }
    if (p->max_snippet_scr.end_i) {
        end = p->max_snippet_scr.end_i;
    } else if(p->nwrds) {
        end = p->nwrds;
    } else {
        printf("Empty paragraph.");
        return;
    }
    for (i=start;i<end;i++) {
        printed = 0;
        
        /* remove any punctuations before checking */
        punc = '\0';
        len_w = strlen(p->words[i]);
        if (strchr(TERM_PUNCT, p->words[i][len_w-1])) {
            punc = p->words[i][len_w-1];
            p->words[i][len_w-1] = '\0';
            len_w--;
        }
        
        /* run through queries and check */
        /* print out matched words */
        for (j=1;j<n_que;j++) {
            if (strlen(queries[j])==len_w &&
                !strncasecmp(p->words[i],queries[j],len_w)) {
                printf("<font color=\"red\"><b>%s</b></font>", p->words[i]);
                /* if exist, print out punctuation outside the tags */
                if (punc) {
                    printf("%c", punc);
                    p->words[i][len_w] = punc;
                }
                printf("\n");
                printed=1;
                break;
            }
        }
        /* print unmatched words */
        if (!printed) {
            /* if exist, put punctuation back in */
            if (punc) {;
                p->words[i][len_w] = punc;
            }
            printf("%s\n", p->words[i]);
        }
    }
    /* end of the snippet */
    if (punc!='.' && punc!='?') {
        printf(" ... ");
    }
    /* snippet's score */
    printf("[%.1f]\n", p->max_snippet_scr.score);
}
