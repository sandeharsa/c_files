/*//////////////////////////////////////////////////////////////////
//                                                                //
//                        Assignment 1                            //
//                                                                //
//     Assignment submission for subject:                         //
//        COMP10002 Foundation of Algorithms                      //
//                                                                //
//  Created by Sande Harsa on 09/10/12.                           //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>

/* constants and fixed strings */

/* HTML formatting strings */
#define TOP "<html><head><title>Algorithms are Fun!</title></head>" \
"<body><h3>Algorithms are Fun!</h3><ol>"
#define PAR "<p><li>"
#define BOT "</ol></body></html>"

/* maximum number of characters per word */
#define MAX_WORD_LEN 23
/* maximum number of words per paragraph */
#define MAX_PARA_LEN 10000

/* return code from get_word if end of paragraph found */
#define PARA_END 1
/* return code from get_word if a word successfully found */
#define WORD_FND 2

/* terminating punctuation that may follow a word */
#define TERM_PUNCT ".,;:!?"
/* the string the separates paragraphs */
#define PARA_SEPARATOR "\n========"

/* maximum words in any output snippet */
#define MAX_SNIPPET_LEN 30
/* minimum words in any output snippet */
#define MIN_SNIPPET_LEN 20

/* maximum terms allowed on command line */
#define MAX_TERMS 50

/* return code from get_paragraph if paragraph found */
#define PARA_FND 1

/* type definitions */

typedef char word_t[MAX_WORD_LEN+1];

typedef struct {
    float score;
    word_t start_wrd;
    word_t end_wrd;
} snippet_t;

typedef struct {
	int nwrds;
	word_t words[MAX_PARA_LEN];
	snippet_t max_snippet_scr;
} para_t;

/* function prototypes */

void	start_output();
void	new_paragraph();
void	end_output();
int	get_word(word_t w, int limit);
int get_paragraph(para_t *p, int limit);
void	put_word(word_t w);
void	put_paragraph(para_t p);
void    put_highlight(para_t p, int, char**);
void    snippet_scoring(para_t *p, int, char**);

/* where it all happens */

int
main(int argc, char **argv) {
    para_t onepar;
    start_output();
    while (get_paragraph(&onepar, MAX_PARA_LEN)!=EOF) {
        new_paragraph();
        put_paragraph(onepar);
        /*put_highlight(onepar, argc, argv);*/
        /*snippet_scoring(&onepar, argc, argv);
        printf("[%.1f] ", onepar.max_snippet_scr.score);
        printf("[%s] ", onepar.max_snippet_scr.start_wrd);
        printf("[%s]", onepar.max_snippet_scr.end_wrd); */
    }
	end_output();
	return 0;
}

/* prelude output, to set up the HTML file */

void
start_output() {
	printf("%s\n", TOP);
}

/* start a new output paragraph in the HTML file */

void
new_paragraph() {
	printf("%s\n", PAR);
}

/* and close off the HTML output */

void
end_output() {
	printf("%s\n", BOT);
}

/* extract a single word out of the standard input, but not
 more than "limit" characters in total. One character of
 sensible trailing punctuation is retained.  Argument array
 w must be limit+1 characters or bigger. */

int
get_word(word_t w, int limit) {
	int c, i, len_w=limit;
	/* first, skip over any non alphanumerics */
	while ((c=getchar())!=EOF && !isalnum(c) && c!='\n') {
		/* do nothing more */
	}
	if (c==EOF) {
		return EOF;
	}
	/* ok, first character of next word has been found */
	*w = c;
	w += 1;
	limit -= 1;
    while (limit>0 && (c=getchar())!=EOF && (isalnum(c) || c=='=')) {
		/* another character to be stored */
		*w = c;
		w += 1;
		limit -= 1;
	}
	/* take a look at that next character, is it trailing
     punctuation? */
	if (strchr(TERM_PUNCT, c) && (limit>0)) {
		/* yes, it is */
		*w = c;
		w += 1;
		limit -= 1;
	}
    
    /* now close off the string */
	*w = '\0';
    
    /* Check whether it is a paragraph seperator or not */
    if (!strcmp((w-(len_w-limit)),PARA_SEPARATOR) && c=='\n') {
        return PARA_END;
    } else if (*(w-(len_w-limit))=='\n') {
        w = w-(len_w-limit)
        for (i=0;i<(len_w-limit);i++) {
            
        }
    }
    
    return WORD_FND;
}

/* Extract a paragraph from a text */
int
get_paragraph(para_t *p, int limit) {
    word_t oneword;
    (*p).nwrds = 0;
    int ctrl;
    /* Iterate over the input by get_word */
    while((*p).nwrds<limit && (ctrl=get_word(oneword, MAX_WORD_LEN))!=EOF) {
        if (ctrl == PARA_END) {
            return PARA_FND;
        } else {
            strcpy((*p).words[(*p).nwrds],oneword);
            (*p).nwrds += 1;
        }
    }
    return EOF;
}

/* write out a non-bold word */
void
put_word(word_t w) {
	printf("%s\n", w);
}
                    
/* write out paragraphs */
void
put_paragraph(para_t p) {
    int i;
    /* Print out a snippet sized paragraph */
    for (i=0;i<MAX_SNIPPET_LEN && i<p.nwrds;i++) {
        put_word(p.words[i]);
    }
}

/* write out bold & colored queries */
void
put_highlight(para_t p, int n_que, char **queries) {
    int i, j, printed, len_w;
    char punc;
    for (i=0;/*i<MAX_SNIPPET_LEN && */i<p.nwrds;i++) {
        printed = 0;
        
        /* remove any punctuations before checking */
        punc = '\0';
        len_w = strlen(p.words[i]);
        if (strchr(TERM_PUNCT, p.words[i][len_w-1])) {
            punc = p.words[i][len_w-1];
            p.words[i][len_w-1] = '\0';
            len_w--;
        }
        
        /* run through queries and check */
        /* print out matched words */
        for (j=1;j<n_que;j++) {
            if (strlen(queries[j])==len_w &&
                !strncasecmp(p.words[i],queries[j],len_w)) {
                printf("<font color=\"red\"><b>%s</b></font>", p.words[i]);
                /* if exist, print out punctuation outside the tags */
                if (punc) {
                    printf("%c", punc);
                }
                printf("\n");
                printed=1;
                break;
            }
        }
        
        /* print unmatched words */
        if (!printed) {
            /* if exist, put punctuation back in */
            if (punc) {;
                p.words[i][len_w] = punc;
            }
            printf("%s\n", p.words[i]);
        }
    }
}

void
snippet_scoring (para_t *p, int n_que, char **queries) {
    int i, j, k, q, len_w;
    float cur_scr=0;
    char punc;
    int word_found[n_que-1];
    int punc_found[p->nwrds];
    memset(punc_found, 0, (p->nwrds)*4);
    p->max_snippet_scr.score = 0;
    /* iterate over possible starting words for the snippets */
    for (i=0;(i+MIN_SNIPPET_LEN)<p->nwrds;i++) {
        /* iterate over 10 possible words for the end word */
        for (j=(i+MIN_SNIPPET_LEN);j<=MAX_SNIPPET_LEN;j++) {
            cur_scr = 0;
            memset(word_found, 0, (n_que-1)*4);
            /* add 0.9 if the snippets start with the first word */
            if (i==0) {
                cur_scr += 0.9;
            } else if (punc_found[i-1]) {
                cur_scr += 0.9;
                printf("0.9 %s %s", p->words[i], p->words[i+1]);
            }
            /* iterate over the snippet itself */
            for (k=i;k<j;k++) {
                /* remove any punctuations before checking */
                punc = '\0';
                len_w = strlen(p->words[k]);
                if (strchr(TERM_PUNCT, p->words[k][len_w-1])) {
                    punc = p->words[k][len_w-1];
                    p->words[k][len_w-1] = '\0';
                    len_w--;
                    /* record the word that is punctuated (for scoring) */
                    punc_found[k] = 1;
                }
                /* run through queries and check */
                for (q=1;q<n_que;q++) {
                    if (strlen(queries[q])==len_w &&
                        !strncasecmp(p->words[k],queries[q],len_w)) {
                        /* add 2.4 when a new word is found,
                            add 1.0 if it is a repeated word */
                        if (!word_found[(q-1)]) {
                            cur_scr += 2.4;
                            word_found[q-1] = 1;
                        } else if (word_found[(q-1)]){
                            cur_scr += 1.0;
                        }
                        break;
                    }
                }
                /* put the punctuation back in */
                if (punc) {
                    p->words[k][len_w+1] = punc;
                }
            }
            /* add 0.7 if the last word is punctuated */
            if (punc) {
                cur_scr += 0.7;
            }
            /* compare snippet scores */
            if (cur_scr>(p->max_snippet_scr.score)) {
                p->max_snippet_scr.score = cur_scr;
                strcpy(p->max_snippet_scr.start_wrd,p->words[i]);
                strcpy(p->max_snippet_scr.end_wrd,p->words[k-1]);
            }
        }
    }
}





