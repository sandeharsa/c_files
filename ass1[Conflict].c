/* Program to query-biased generate snippets that reflect the content
 of a sequence of paragraphs of text
 
 Skeleton written by Alistair Moffat, August 2012.
 Modified by XXXX, August 2012.
 */


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>

/* constants and fixed strings */

/* HTML formatting strings */
#define TOP "<html><head><title>Algorithms are Fun!</title></head>" \
"<body><h3>Algorithms are Fun!</h3><ol>"
#define PAR "<p><li>"
#define BOT "</ol></body></html>"

/* maximum number of characters per word */
#define MAX_WORD_LEN 23
/* maximum number of words per paragraph */
#define MAX_PARA_LEN 10000

/* return code from get_word if end of paragraph found */
#define PARA_END 1
/* return code from get_word if a word successfully found */
#define WORD_FND 2

/* terminating punctuation that may follow a word */
#define TERM_PUNCT ".,;:!?"
/* the string the separates paragraphs */
#define PARA_SEPARATOR "\n========\n"

/* maximum words in any output snippet */
#define MAX_SNIPPET_LEN 30
/* minimum words in any output snippet */
#define MIN_SNIPPET_LEN 20

/* maximum terms allowed on command line */
#define MAX_TERMS 50

/* type definitions */

typedef char word_t[MAX_WORD_LEN+1];

typedef struct {
	int nwrds;
	word_t words[MAX_PARA_LEN];
	/* you can add extra fields here if wish */
} para_t;

/* you can add extra types here if you wish */

/* function prototypes */

void	start_output();
void	new_paragraph();
void	end_output();
int	get_word(word_t w, int limit);
void	put_word(word_t w);

/* where it all happens */

int
main(int argc, char **argv) {
	word_t oneword;
	start_output();
	new_paragraph();
	while (get_word(oneword, MAX_WORD_LEN) != EOF) {
        if (strcmp(oneword, PARA_SEPARATOR)) {
            put_word(oneword);
        }
    }
	end_output();
	return 0;
}

/* prelude output, to set up the HTML file */

void
start_output() {
	printf("%s\n", TOP);
}

/* start a new output paragraph in the HTML file */

void
new_paragraph() {
	printf("%s\n", PAR);
}

/* and close off the HTML output */

void
end_output() {
	printf("%s\n", BOT);
}

/* extract a single word out of the standard input, but not
 more than "limit" characters in total. One character of
 sensible trailing punctuation is retained.  Argument array
 w must be limit+1 characters or bigger. */

int
get_word(word_t w, int limit) {
	int c, cw=0;
    
	/* first, skip over any non alphanumerics */
	while ((c=getchar())!=EOF && !isalnum(c) && c!='\n') {
		/* do nothing more */
	}
	if (c==EOF) {
		return EOF;
	}
    
	/* ok, first character of next word has been found */
	*w = c;
	w += 1;
    cw += 1;
	limit -= 1;
    
    while (limit>0 && (c=getchar())!=EOF && (isalnum(c) || c=='=' || c=='\n')) {
		/* another character to be stored */
		*w = c;
		w += 1;
        cw += 1;
		limit -= 1;
	}
    
	/* take a look at that next character, is it trailing
     punctuation? */
	if (strchr(TERM_PUNCT, c) && (limit>0)) {
		/* yes, it is */
		*w = c;
		w += 1;
        cw += 1;
		limit -= 1;
	}
    
	/* now close off the string */
	*w = '\0';
    
    /* Check whether it is a paragraph seperator or not */
    if (strcmp((w-cw),PARA_SEPARATOR)==0) {
        return PARA_END;
    }
    
    return WORD_FND;
}

/* write out a non-bold word */

void
put_word(word_t w) {
	printf("%s\n", w);
}
