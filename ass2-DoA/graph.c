/* 
COMP20007 Design of Algorithms 

Module to supply a graph data structure for Assignment 2.

Author: Andrew Turpin (aturpin@unimelb.edu.au)
Date: April 2013

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "vector.h"
#include "vertex.h"
#include "listops.h"
#include "graph.h"

// create an empty graph
Graph *
create_graph() {
    Graph *g = (Graph *)malloc(sizeof(Graph));
    g->m = 0;
    g->vertices = vector_new();
    return g;
}

// print graph
void
print_graph(Graph *g) {
    printf("************** GRAPH n=%d m=%d\n",vector_size(g->vertices), g->m);
    for (int i = 0 ; i < vector_size(g->vertices) ; i++) {
        Vertex *v = VERTEX(g, i);
        printf("%3d txt= %s in= %3d out= %3d : ",i, v->text, v->in_degree, v->out_degree);
        Label *j;
        for (j = begin_iterator(v->neighbours) ; j != NULL ; ) {
            printf(LIST_PRINT_FORMAT, *j);
            j = step_iterator(v->neighbours);
        }
        printf("\n");
    }
    fflush(stdout);
}

/*
** Return number of edges in graph
*/
unsigned int get_number_of_edges(Graph *g) { return g->m; }

/*
** Add vertex to graph with text s.
** Does not check for duplicate text!
** Does not make a copy of *s.
** SIDE EFFECTS: may allocate space for vertices vector
*/
Label
add_vertex(Graph *g, char *s) {
    Vertex *v = (Vertex *)malloc(sizeof(Vertex));
    assert(v != NULL);
    v->in_degree  = 0;
    v->out_degree = 0;
    v->text = s;
    v->neighbours = make_empty_list();

    int index = vector_push_back(g->vertices, v);
    assert(index != -1);

    return index;
}

/*
** Add edge(u,v) to graph
** ASSUMES: graph[u] and graph[v] are initialised.
** SIDE EFFECTS: alters graph
*/
void
insert_edge(Graph *g, Label u, Label v) {
    assert((int)u < vector_size(g->vertices));
    assert((int)v < vector_size(g->vertices));
    VERTEX(g,u)->out_degree++;
    VERTEX(g,v)->in_degree++;
    insert_at_foot(VERTEX(g, u)->neighbours, v);
    g->m++;
}

/*
** Find a vertex in path that has out_degree > 0, set it to middle
** Put all vertices before middle in *before 
** and all after middle in *after.
** If there is no vertex with out_degree > 0, set middle to g->n+1 and before and after not defined.
*/
void
split_path(Graph *g, LabelList path, LabelList *before, Label *middle, LabelList *after) {
    Label *i;

    for (i = begin_iterator(path); i != NULL && VERTEX(g, *i)->out_degree == 0; )
        i = step_iterator(path);

    if (i == NULL) {
        *middle = vector_size(g->vertices) + 1;
    } else {
        *middle = *i;
        split_list(path, before, after);
    }
}

/*
** Print strings for each vertex in path
*/
void
print_path(Graph *g, LabelList path) {
    if (list_size(path) < 1)
        return;
    Label *i = begin_iterator(path); 
    int k = strlen(VERTEX(g, *i)->text);
    printf("%s", VERTEX(g, *i)->text);
    i = step_iterator(path);
    while (i != NULL) {
        printf("%c",VERTEX(g, *i)->text[k-1]);
        i = step_iterator(path);
    }
    printf("\n");
}

/**************************
* Students mod below here 
    mod by Sande Harsa 567642*
***************************/

// find vertex with out_degree == 0
// if no such vertex, return g->n + 1
Label
find_end(Graph *g) {
    // Iterate over the vertices in the graph 
    for (int i = 0 ; i < vector_size(g->vertices) ; i++) {
        Vertex *v = VERTEX(g, i); // access the i-th vertex, i.e. v[i]
        if(v->out_degree == 0) {
            return i;
        }
    }
    return vector_size(g->vertices) + 1;
}

// find vertex with in_degree == 0
// if no such vertex, return g->n + 1
Label
find_start(Graph *g) {
    // Iterate over the vertices in the graph 
    for (int i = 0 ; i < vector_size(g->vertices) ; i++) {
        Vertex *v = VERTEX(g, i); // access the i-th vertex, i.e. v[i]
        if(v->in_degree == 0) {
            return i;
        }
    }
    return vector_size(g->vertices) + 1;
}

/*
** INPUT Graph g (assumed to be a de Brujin graph) 
**       and the label of the start vertex
** RETURN list of vertices that form a cycle from start.
**        The list includes start at the beginning, but not the end
**        If there is no cycle, return empty list
** ASSUMES Every vertex with an out edge will start a cycle
**         (i.e assume the input will always be a de Brujin graph)
** SIDE EFFECT Modify the graph. The edges between each vertex 
**             that is included in the cycle will be removed from
**             the graph. However, g->m is not changed, thus it
**             represents the original number of edges.
*/
LabelList
find_cycle(Graph *g, Label start) {
    Vertex *v;
    LabelList path = make_empty_list();
    v = VERTEX(g, start);
    if(v == NULL || v->out_degree == 0)
        return path;
    // Start the cycle by inserting the start vertex
    insert_at_foot(path, start);
    Label next = remove_head(v->neighbours);
    v->out_degree--;
    /* Iterate through the vertices until the next Vertex 
       is the starting vertex (i.e a cycle has been found) */
    while(next != start) {
        insert_at_foot(path, next);
        v = VERTEX(g, next);
        next = remove_head(v->neighbours);
        v->out_degree--;
        v->in_degree--; // the effect of the previous loop
    }
    return path;
}

/*
** INPUT Graph g (assumed to be a de Brujin graph) 
**       and the label of the start vertex
** RETURN: NULL if no Euler path
**         List of vertices that form a Eulerian path otherwise.
** ASSUMES Every vertex with an out edge will start a cycle
**         (i.e assume the input will always be a de Brujin graph)
** SIDE EFFECT If a eulerian path is found, all edges on the 
**             graph will be removed
*/
LabelList 
find_euler_path(Graph *graph, Label start) {
    LabelList before, after;
    Label middle;
    LabelList path = make_empty_list();
    // Find the initial cycle to work from
    path = find_cycle(graph, start);
    /* While the path does not include all the edges,
    ** 1. Use split path
    ** 2. find_cycle that starts from the middle vertex
    ** 3. join these lists: before, new middle cycle, and
    **    after. Also insert middle to include its last iteration.
    */
    while(list_size(path) != get_number_of_edges(graph)) {
        split_path(graph, path, &before, &middle, &after);
        path = join_lists(before, find_cycle(graph, middle));
        insert_at_foot(path, middle); //insert the last iteration of middle
        path = join_lists(path, after);
    }
    return path;
}

