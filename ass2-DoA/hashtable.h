/* Author J. Zobel, April 2001.
   Permission to use this code is freely granted, provided that this
   statement is retained. 

   Modified Andrew Turpin, April 2013 for COMP20007.
*/

#define TSIZE   (1 << 14)
#define SEED    1159241
#define HASHFN  bitwisehash

typedef struct hashrec {
    char *string;           // text label of vertex
    Label  vertex;          // vertex number
    struct hashrec *next;
} HASHREC;


HASHREC ** inithashtable(void);                         // create empty hash table
HASHREC  * hashsearch(HASHREC **ht, char *c);           // Search ht for c, return record if found, else NULL 
void       hashinsert(HASHREC **ht, char *c, Label v);  // insert v with string c into ht
unsigned int bitwisehash(char *c, int tsize, unsigned int seed);
int ccmp(char *c1, char *c2);
void       dumpHashTable(HASHREC **ht);
