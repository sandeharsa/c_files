/*
** Implement a C version of subset of C++ vector.
**
** Author: Andrew Turpin
** Date: Mon 29 Apr 2013 21:57:23 EST
**
*/

#include <stdlib.h>

#define VECTOR_SET_FAIL 0
#define VECTOR_SET_PASS 1

typedef struct {
    int maxSize;    // max index allowed + 1
    int size;       // num elements in vec == index of next free slot
    void **V;       // the array of ptrs to elements
} Vector;

Vector *vector_new();                      // return new vector with no elements
int vector_resize(Vector *v, int n);       // resize vector 
void *vector_get(Vector *v, int i);        // return v[i] or NULL
int vector_push_back(Vector *v, void *p);  // add p to end of v and return index of p or -1
int vector_size(Vector *v);                // reutrn number of elements in v
