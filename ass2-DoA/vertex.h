/* 
 COMP20007 Design of Algorithms 

 Type of a vertex label

 Author: Andrew Turpin (aturpin@unimelb.edu.au)
 Date: April 2013

*/

typedef unsigned int Label;
