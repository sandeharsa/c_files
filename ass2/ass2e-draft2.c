/*//////////////////////////////////////////////////////////////////
//                                                                //
//                        Assignment 2                            //
//                                                                //
//     Assignment submission for subject:                         //
//        COMP10002 Foundation of Algorithms                      //
//                                                                //
//  Created by Sande Harsa on 13/10/12.                           //
//  User Name: sharsa                                             //
//                                                                //
w//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include "listops.h"

#define MAXBYTES 100000
#define NASCII 127

/* typedef(s) */
typedef struct {
    int length;
    int offset;
} echar_t; /* encoded char */

typedef int data_t;

typedef list_t* table_t[NASCII];

/* function prototypes */
int read_input(char[]);
table_t *make_empty_table();
void table_insertion(table_t*, char*, int);
int encoding(char[], table_t*, echar_t[], int);
void write_output(echar_t[], int);

int
main(int argc, char **argv) {
    int limit;
    char S[MAXBYTES];
    echar_t encoded_msg[MAXBYTES];
    table_t *table;
    table = make_empty_table();
    limit = read_input(S);
    limit = encoding(S, table, encoded_msg, limit);
    write_output(encoded_msg, limit);
    return 0;
}

/* Read in a string input from a file */
int
read_input(char S[]) {
    int i;
    for(i=0;(S[i]=getchar())!=EOF;i++);
    S[i]='\0';
    return i;
}

/* Initialize a table_t */
table_t
*make_empty_table() {
    int i;
    table_t *t;
    t = malloc(sizeof(*t));
    assert(t);
    for(i=0;i<NASCII;i++) {
        (*t)[i] = make_empty_list();
    }
    return t;
}

/* Insert all the characters from the given string into the table */
void
table_insertion(table_t *t, char S[], int limit) {
    int i, code;
    data_t *insert;
    for(i=0;i<limit;i++) {
        code = (int)S[i];
        insert = malloc(sizeof(*insert));
        assert(insert);
        *insert = (data_t)i;
        insert_at_foot((*t)[code], insert);
    }
}


/* encoder function */
int
encoding(char S[], table_t *t, echar_t E[], int limit) {
    int i, j, k, imax=-1, n=0, count=0;
    int code, *cpos, matched;
    data_t *insert;
    node_t *current;
    for(i=0;i<limit;i++) {
        matched=0;
        code = (int)S[i];
        if(is_empty_list((*t)[code])) {
            E[n].offset = 0;
            E[n].length = code;
            insert = malloc(sizeof(*insert));
            *insert = (data_t)i;
            insert_at_head((*t)[code], insert);
            imax++;
        } else {
            matched=1;
            E[n].length = 0;
            current = (*t)[code]->head;
            while(current) {
                count = 0;
                cpos = current->data;
                for(j=(*cpos), k=i;j<i && k<limit;j++,k++) {
                    if(S[j]!=S[k]) break;
                    count++;
                    if(k>imax) {
                        imax = k;
                        code = (int)S[imax];
                        insert = malloc(sizeof(*insert));
                        *insert = (data_t)imax;
                        insert_at_head((*t)[code], insert);
                    }
                }
                if(count>E[n].length) {
                    E[n].length = count;
                    E[n].offset = i-(*cpos);
                }
                current = current->next;
            }
        }
        if(matched) {
            i += E[n].length-1;
        }
        n++;
    }
    return n;
}

/* Write out the compressed bytes */
void write_output(echar_t E[], int limit) {
    int i;
    for(i=0;i<limit;i++) {
        printf("%d,%d\n", E[i].offset, E[i].length);
    }
}
