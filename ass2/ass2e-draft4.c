/*//////////////////////////////////////////////////////////////////
//                                                                //
//                        Assignment 2                            //
//                                                                //
//     Assignment submission for subject:                         //
//        COMP10002 Foundation of Algorithms                      //
//                                                                //
//  Created by Sande Harsa on 13/10/12.                           //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include "listops.h"

#define MAXBYTES 1000000
#define NASCII 128

/* typedef(s) */
typedef struct {
    int length;
    int offset;
} echar_t; /* encoded char */

typedef list_t* table_t[NASCII];

/* function prototypes */
int read_input(char[]);
table_t *make_empty_table();
void table_insertion(table_t*, char*, int);
int encoding(char[], table_t*, int);

int
main(int argc, char **argv) {
    int limit;
    char S[MAXBYTES];
    table_t *table;
    table = make_empty_table();
    limit = read_input(S);
    /*table_insertion(table, S, limit);*/
    limit = encoding(S, table, limit);
    return 0;
}

/* Read in a string input from a file */
int
read_input(char S[]) {
    int i;
    for(i=0;(S[i]=getchar())!=EOF;i++);
    S[i]='\0';
    return i;
}

/* Initialize a table_t */
table_t
*make_empty_table() {
    int i;
    table_t *t;
    t = malloc(sizeof(*t));
    assert(t);
    for(i=0;i<NASCII;i++) {
        (*t)[i] = make_empty_list();
    }
    return t;
}

/* Insert all the characters from the given string into the table */
void
table_insertion(table_t *t, char S[], int limit) {
    int i, code;
    for(i=0;i<limit;i++) {
        code = (int)S[i];
        insert_at_foot((*t)[code], i);
    }
}


/* encoder function */
int
encoding(char S[], table_t *t, int limit) {
    int i, j, k, n=0, count=0;
    int code, cpos, matched;
    node_t *current;
    echar_t temp;
    for(i=0;i<limit;i++) {
        matched=0;
        code = (int)S[i];
        if(is_empty_list((*t)[code])) {
            temp.offset = 0;
            temp.length = code;
        } else {
            matched=1;
            temp.length = 0;
            current = (*t)[code]->head;
            while(current) {
                count = 1;
                cpos = current->data;
                for(j=cpos+1, k=i+1;j<i && k<limit;j++,k++) {
                    if(S[j]!=S[k]) break;
                    count++;
                }
                if(count>temp.length) {
                    temp.length = count;
                    temp.offset = i-cpos;
                }
                current = current->next;
            }
        }
        printf("%d,%d\n", temp.offset, temp.length);
        insert_at_head((*t)[code], i);
        if(matched) {
            for(j=i+1;j<(i+temp.length);j++) {
                code = (int)S[j];
                insert_at_head((*t)[code], j);
            }
            i += temp.length-1;
        }
        n++;
    }
    return n;
}
