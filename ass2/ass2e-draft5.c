/*//////////////////////////////////////////////////////////////////
//                                                                //
//                        Assignment 2                            //
//                                                                //
//     Assignment submission for subject:                         //
//        COMP10002 Foundation of Algorithms                      //
//                                                                //
//  Created by Sande Harsa on 13/10/12.                           //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
/* Include the list operations from the lecture */
#include "listops.h"

#define MAXBYTES 1000000
#define NASCII 256
#define LITERAL 0

/* typedef(s) */
typedef struct {
    int length;
    int offset;
} echar_t; /* encoded char */

typedef list_t* table_t[NASCII][NASCII];

/* function prototypes */
int read_input(char[]);
table_t *make_empty_table();
int encoding(char[], table_t*, int);

int
main(int argc, char **argv) {
    int limit;
    char S[MAXBYTES];
    table_t *table;
    table = make_empty_table();
    limit = read_input(S);
    encoding(S, table, limit);
    return 0;
}

/* Read in a string input from a file */
int
read_input(char S[]) {
    int i;
    for(i=0;(S[i]=getchar())!=EOF;i++);
    S[i]='\0';
    return i;
}

/* Initialize a 2D table */
/* The table consists of:
 - Row indices as positions of each occurance of characters from the input file.
 - each row has a column:
 - Column indices as the next character after each row character
 - Each of the cells contain a pointer to a list.
 - The list contains position of the characters (represented by the
 indices of the rows
 The decision to use a 2D table is because a single array with ASCII indices
 is not sufficient to make a 0.0x running time. However the memory use does
 expand quite a bit.
 Moreover, by using lists, the memory use is flexible. For each list memory
 use is the exact number of items in the list. The sum of numbers in all of the
 lists will be n, the length of the input file */
table_t
*make_empty_table() {
    int i, j;
    table_t *t;
    t = malloc(sizeof(*t));
    for(i=0;i<NASCII;i++) {
        for(j=0;j<NASCII;j++) {
            (*t)[i][j] = make_empty_list();
        }
    }
    return t;
}

/* Encoder function */
/* Encoding process starts up by filling in cells in the table until it found a
 same character as the one already in the table and calculate the longest
 length with the corresponding offset */
int
encoding(char S[], table_t *t, int limit) {
    int i, j, k, n=0, count=0;
    int code, cnext, cpos, matched, empty;
    node_t *current;
    echar_t temp;
    /* Iterate through the input string */
    for(i=0;i<limit;i++) {
        matched=0;
        empty=1;
        /* Assign the ASCII code of the current char */
        code = (int)S[i];
        /* Assign the ASCII code of the char next to the current char */
        if(i<limit-1)
            cnext = (int)S[i+1];
        else
            cnext = 0;
        /* Check if the array of list for the current ASCII is empty */
        for(j=0;j<NASCII;j++) {
            if(!is_empty_list((*t)[code][j])) {
                empty=0;
                break;
            }
        }
        if(empty) {
            /* First occurance of the char */
            temp.offset = LITERAL;
            temp.length = code;
        } else {
            matched=1;
            temp.length = 0;
            current = (*t)[code][cnext]->head;
            /* if there are no pair of code+cnext, find a single char of code */
            if(!current) {
                temp.length=1;
                for(j=i-1;j>=0;j--) {
                    if (S[i]==S[j]) {
                        temp.offset=i-j;
                        break;
                    }
                }
            } else {
                /* find the longest offset length from the current char */
                while(current) {
                    count = 1;
                    cpos = current->data;
                    for(j=cpos+1, k=i+1;j<i && k<limit;j++,k++) {
                        if(S[j]!=S[k]) break;
                        count++;
                    }
                    if(count>temp.length) {
                        temp.length = count;
                        temp.offset = i-cpos;
                    }
                    current = current->next;
                }
            }
        }
        /* prints the offset,length straightaway as it is no longer needed */
        printf("%d,%d\n", temp.offset, temp.length);
        if(i<limit-1) {
            insert_at_head((*t)[code][cnext], i);
        }
        /* insert all of the characters that are in between the current char
         and the char in position current+length. Then, skip those chars in the
         outer iteration */
        if(matched) {
            for(j=i+1;j<(i+temp.length) && j<limit-1;j++) {
                code = (int)S[j];
                cnext = (int)S[j+1];
                insert_at_head((*t)[code][cnext], j);
            }
            i += temp.length-1;
        }
        n++;
    }
    return n;
}
