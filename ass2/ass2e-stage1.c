/*//////////////////////////////////////////////////////////////////
//                                                                //
//                        Assignment 2                            //
//                                                                //
//     Assignment submission for subject:                         //
//        COMP10002 Foundation of Algorithms                      //
//                                                                //
//  Created by Sande Harsa on 13/10/12.                           //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include "treeops.h"

#define MAXBYTES 1000000

/* typedef(s) */
typedef struct {
    int offset;
    int length;
} echar_t;

/* function prototypes */
int read_input(char[]);
int encoding(char[], echar_t[], int);
void write_output(echar_t[], int);

int
main(int argc, char **argv) {
    int limit;
    char S[MAXBYTES];
    echar_t encoded_msg[MAXBYTES/2];
    limit = read_input(S);
    limit = encoding(S, encoded_msg, limit);
    write_output(encoded_msg, limit);
    return 0;
}

int
read_input(char S[]) {
    int i;
    for(i=0;(S[i]=getchar())!=EOF;i++);
    S[i]='\0';
    return i;
}

int
encoding(char S[], echar_t E[], int limit) {
    int i, j, x, y;
    int matched, cur=0, max_length=0, count=0;
    int current_offset, max_offset;
    i=0;
    while(i<limit) {
        matched=0;
        max_length = 0;
        for(j=0;j<i;j++) {
            count=0;
            if(S[i]==S[j]) {
                current_offset = i-j;
                count++;
                matched=1;
                for(x=i+1,y=j+1;x<limit && y<i;x++,y++) {
                    if(S[x]==S[y]) {
                        count++;
                    } else {
                        break;
                    }
                }
            }
            if(count>=max_length) {
                max_length = count;
                max_offset = current_offset;
            }
        }
        if(matched) {
            E[cur].length = max_length;
            E[cur].offset = max_offset;
            i += max_length;
        } else {
            E[cur].length = (int)S[i];
            E[cur].offset = 0;
            i++;
        }
        cur++;
    }
    return cur;
}



void write_output(echar_t E[], int limit) {
    int i;
    for(i=0;i<limit;i++) {
        printf("%d,%d\n", E[i].offset, E[i].length);
    }
}
