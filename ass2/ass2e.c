/*//////////////////////////////////////////////////////////////////
//                                                                //
//                        Assignment 2                            //
//                                                                //
//     Assignment submission for subject:                         //
//        COMP10002 Foundation of Algorithms                      //
//                                                                //
//  Created by Sande Harsa on 13/10/12.                           //
//  User Name: sharsa                                             //
//                                                                //
//////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
/* Include the list operations from the lectures */
#include "listops.h"

#define MAXBYTES 1000000
#define NASCII 256
#define LITERAL 0
#define EMPTY_ARRAY -1

/* typedef(s) */
typedef struct {
    int length;
    int offset;
} echar_t; /* encoded char */

typedef struct {
    list_t* lists[NASCII];
    int last; /* last item that was inserted to one of the lists */
} row_t; /* table row */

typedef row_t table_t[NASCII];

/* function prototypes */
int read_input(unsigned char[]);
table_t *make_empty_table();
void encoding(unsigned char[], table_t*, int);

int
main(int argc, char **argv) {
    int limit;
    /* using unsigned char to read binary files and also save memory
     (compared to unsigned int) */
    unsigned char S[MAXBYTES];
    table_t *table;
    table = make_empty_table();
    limit = read_input(S);
    encoding(S, table, limit);
    return 0;
}

/* Read in the input file */
int
read_input(unsigned char S[]) {
    int i;
    unsigned c;
    for(i=0;(c=getchar())!=EOF;i++) {
        S[i] = (unsigned char)c;
    }
    return i;
}

/* Initialize a 2D table */
/* The table consists of:
 - Row indices as positions of each occurance of characters from the input file.
 - each row contain an array of lists (the columns)
 - each row contain the last data inserted into the array
 - Column indices as the next character after each row character
 - Each of the cells contain a pointer to a list.
 - The list contains position of the characters (represented by the
 indices of the rows
 The decision to use a 2D table was because a single array with ASCII indices
 is not sufficient to make a 0.1x running time (on a Macbook Pro). However the
 memory use does expand quite a bit.
 Moreover, by using lists, the memory use is flexible. For each list memory
 use is the exact number of items in the list. The sum of numbers in all of the
 lists will be n, which is the length of the input file.
 The use of list instead of trees is because the fact that the data inserted
 into the list will be in assending order. This means that if a tree structure
 was used, a 'stick' will be constructed and memory usage of one pointer for
 each node will be wasted.
 At the end, the memory use will be 256*256*sizeof(int) + input file size.
 However, the running time is now almost 1/8 comparing it with a 1D array table.
 (1D array: 0.5-0.6s, 2D array: 0.07s -- on a Macbook Pro) */
table_t
*make_empty_table() {
    int i, j;
    table_t *t;
    t = malloc(sizeof(*t));
    for(i=0;i<NASCII;i++) {
        for(j=0;j<NASCII;j++) {
            (*t)[i].lists[j] = make_empty_list();
            (*t)[i].last = EMPTY_ARRAY;
        }
    }
    return t;
}

/* Encoder function */
/* Encoding process starts up by filling in cells in the table until it found a
 same character as the one already in the table and calculate the longest
 length with the corresponding offset */
void
encoding(unsigned char S[], table_t *t, int limit) {
    int i, j, k, count=0;
    int code, cnext, cpos, matched, empty;
    node_t *current;
    echar_t temp;
    /* Iterate through the input string */
    for(i=0;i<limit;i++) {
        matched=0;
        empty=1;
        /* Assign the ASCII code of the current char */
        code = (unsigned)S[i];
        /* Assign the ASCII code of the char next to the current char */
        if(i<limit-1) cnext = (unsigned)S[i+1];
        else cnext = 0;
        /* Check if the array of list for the current ASCII is empty */
        if((*t)[code].last==EMPTY_ARRAY) {
            /* First occurance of the char */
            temp.offset = LITERAL;
            temp.length = code;
        } else {
            matched=1;
            temp.length = 0;
            current = (*t)[code].lists[cnext]->head;
            /* if there are no pair of code+cnext, use the last
             occurance of char code */
            if(!current) {
                temp.length=1;
                temp.offset=i-(*t)[code].last;
            }
            /* find the longest offset length from the current char */
            while(current) {
                count = 1;
                cpos = current->data;
                for(j=cpos+1, k=i+1;j<i && k<limit;j++,k++) {
                    if(S[j]!=S[k]) break;
                    count++;
                }
                if(count>temp.length) {
                    temp.offset = i-cpos;
                    temp.length = count;
                }
                current = current->next;
            }
        }
        /* prints the offset,length straightaway as it is no longer needed */
        printf("%d,%d\n", temp.offset, temp.length);
        if(i<limit-1) {
            insert_at_head((*t)[code].lists[cnext], i);
            (*t)[code].last = i;
        }
        /* insert all of the characters that are in between the current char
         and the char in position current+length. Then, skip those chars in the
         outer iteration */
        if(matched) {
            for(j=i+1;j<(i+temp.length) && j<limit-1;j++) {
                code = (unsigned)S[j];
                cnext = (unsigned)S[j+1];
                insert_at_head((*t)[code].lists[cnext], j);
                (*t)[code].last = j;
            }
            i += temp.length-1;
        }
    }
}
