typedef int data_t;

typedef struct node node_t;

struct node {
	data_t data;
	node_t *next;
};

typedef struct {
	node_t *head;
	node_t *foot;
} list_t;

/* prototypes for the functions in this library */
list_t *make_empty_list(void);
int is_empty_list(list_t*);
void free_list(list_t*);
list_t *insert_at_head(list_t*, data_t);
list_t *insert_at_foot(list_t*, data_t);
data_t get_head(list_t*);
list_t *get_tail(list_t*);
