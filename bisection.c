/* The bisection method for finding a root of an equation.
*/
#include <stdio.h>
#include <stdib.h>
#include <math.h>

#define LEFT 0.0
#define RGHT 1.0

#define NOT_CONVERGING (-1)

double f(double);

double bisection(double, double, double, int);

int
main(int argc, char **argv) {
	double x;
	x = bisection(LEFT, RGHT, 1e-6, 1000);
	printf("(main) x = %.10f, f(x) = %12.10f\n", x, f(x));
	x = bisection(LEFT, RGHT, 1e-8, 1000);
	printf("(main) x = %.10f, f(x) = %12.10f\n", x, f(x));
	x = bisection(LEFT, RGHT, 1e-10, 1000);
	printf("(main) x = %.10f, f(x) = %12.10f\n", x, f(x));
	return 0;
}

double
f(double x) {
	return sin(5*x) + cos(10*x) + x*x/10;
}

double
bisection(double x1, double x2, double eps, int limit) {
	double fx1, fx2, mid, fmid;
	int iterations=0;
	fx1 = f(x1);
	fx2 = f(x2);
	while (x2-x1 > eps) {
		iterations = iterations+1;
		if (iterations==limit) {
			exit(NOT_CONVERGING);
		}
		mid = (x1+x2)/2;
		fmid = f(mid);
		if (fx1*fmid < 0) {
			/* root is to left of middle */
			x2 = mid;
			fx2 = fmid;
		} else {
			/* root is to right */
			x1 = mid;
			fx1 = fmid;
		}
	}
	printf("(bisection) eps=%.1e, iterations=%d\n",
			eps, iterations);
	return (x1+x2)/2;
}

/* =====================================================================
   Program written by Alistair Moffat, as an example for the book
   "Programming, Problem Solving, and Abstraction with C", Pearson
   SprintPrint, Sydney, Australia, 2003.

   See http://www.csse.unimelb.edu.au/~alistair/ppsaa/ for further
   information.

   This version prepared January 2012.
   ================================================================== */
