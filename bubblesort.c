/* Read an array, sort it, write it out again.
 */
#include <stdio.h>

#define MAXVALS 10

int read_int_array(int[], int);
void sort_int_array(int[], int);
void print_int_array(int[], int);
void int_swap(int*, int*);

int
main(int argc, char **argv) {
	int numbers[MAXVALS], valid;
	valid = read_int_array(numbers, MAXVALS);
	printf("Before: ");
	print_int_array(numbers, valid);
	sort_int_array(numbers, valid);
	printf("After : ");
	print_int_array(numbers, valid);
	return 0;
}

int
read_int_array(int A[], int maxvals) {
	int n, excess, next;
	printf("Enter as many as %d values, ^D to end\n",
           maxvals);
	n = 0; excess = 0;
	while (scanf("%d", &next)==1) {
		if (n==maxvals) {
			excess = excess+1;
		} else {
			A[n] = next;
			n += 1;
		}
	}
	printf("%d values read into array", n);
	if (excess) {
		printf(", %d excess values discarded", excess);
	}
	printf("\n");
	return n;
}

void
sort_int_array(int A[], int n) {
	int i, didswaps;
	/* assume that A[0] to A[n-1] have valid values */
	didswaps = 1;
	while (didswaps) {
		didswaps = 0;
		for (i=0; i<n-1; i++) {
			if (A[i] > A[i+1]) {
				int_swap(&A[i], &A[i+1]);
				didswaps = 1;
			}
		}
	}
}

void
print_int_array(int A[], int n) {
	int i;
	for (i=0; i<n; i++) {
		printf("%3d", A[i]);
	}
	printf("\n");
}

/* exchange the values of the two variables indicated
 by the arguments */
void
int_swap(int *x, int *y) {
	int t;
	t = *x;
	*x = *y;
	*y = t;
}

/* =====================================================================
 Program written by Alistair Moffat, as an example for the book
 "Programming, Problem Solving, and Abstraction with C", Pearson
 SprintPrint, Sydney, Australia, 2003.
 
 See http://www.csse.unimelb.edu.au/~alistair/ppsaa/ for further
 information.
 
 This version prepared January 2012.
 ================================================================== */