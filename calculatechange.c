/*
//  calculatechange.c
//
//  Calculate the minimum coins needed to form a value of money in cents.
//
//  Created by Sande Harsa on 8/9/12.
//
*/

#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char **argv) {
    int money;
    
    /* Take the input and check it's validity */
    printf("Enter amount in cents:\n");
    scanf("%d", &money);
    if ((money < 0) || (money > 99)) {
        printf("Invalid input. ");
        printf("Please enter a value from 0-99.\n");
        exit(EXIT_FAILURE);
    }
    
    /* Print the first line of the output */
    printf("The coins required to make %d cents are:\n", money);
    
    /* Calculate the change needed by substracting it
     with high to low coin values */
    while (money >= 0) {
        if ((money-50) >= 0) {
            printf("50");
            money -= 50;
        } else if ((money-20) >= 0) {
            printf("20");
            money -= 20;
        } else if ((money-10) >= 0) {
            printf("10");
            money -= 10;
        } else if ((money-5) >= 0) {
            printf("5");
            money -= 5;
        } else if ((money-2) >= 0) {
            printf("2");
            money -= 2;
        } else if ((money-1) >= 0) {
            printf("1");
            money -= 1;
        /* Stop looping and exit the program when the money == 0 */
        } else {
            printf("\n");
            return 1;
        }
        /* Print commas to seperate the coins */
        if (money != 0) {
            printf(", ");
        }
    }
           
    return 0;
}
