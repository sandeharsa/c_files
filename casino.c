#include <stdio.h>

#define ACE 1
#define HIGHCARD 11
#define ITERATIONS 100000
#define WIN1 21
#define WIN1VAL 20
#define WIN2 10
#define FEE 2

int dice(int);
card_t card();

typedef struct {
	int num;
	char symbol;
} card_t;

int
main(int argc, char **argv) {
	int i, sum;
	card_t card;
	int money=0;
	double average;
	for(i=0;i<ITERATIONS;i++) {
		card = card();
		if(card.num > 10) {
			sum = HIGHCARD;
		} else if(card.num == 1) {
			sum = ACE;
		}
		sum += dice(n);
		if(sum==21) {
			money+=WIN1;			
		} else if(sum==20 && card.symbol = 'H') {
			money+=WIN2;
		}
		money -= FEE;
	average = money/ITERATIONS;
	}
	return 0;
}

int
dice(int n) {
	int i, sum=0;
	for(i=0;i<n;i++) {
		sum += ((rand()%6) + 1;
	}
	return sum;
}

card_t
card() {
	card_t card;
	char symbols[] = {'C','H','S','D'}
	card.num = (rand()%13) + 1;
	card.symbol = symbols[(rand()%4)+1)]; 
	return card;
}