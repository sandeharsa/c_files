/*
//  complex_num.c
//  Library of functions using complex numbers.
//
//  Created by Sande Harsa on 10/4/12.
//
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    double re;
    double im;
} complex_t;

complex_t complex_add(complex_t v1, complex_t v2);
complex_t complex_mpy(complex_t v1, complex_t v2);

int
main(int argc, char **argv) {
    complex_t v1, v2, v_add, v_mpy;
    printf("Input the first complex number : ");
    scanf("%lf %lfi", &v1.re, &v1.im);
    printf("Input the second complex number: ");
    scanf("%lf %lfi", &v2.re, &v2.im);
    v_add = complex_add(v1, v2);
    v_mpy = complex_mpy(v1, v2);
    printf("v1 + v2 = %.2f %.2fi\n", v_add.re, v_add.im);
    printf("v1 * v2 = %.2f %.2fi\n", v_mpy.re, v_mpy.im);
    return 0;
}

complex_t
complex_add(complex_t v1, complex_t v2) {
    complex_t v_add;
    v_add.re = v1.re + v2.re;
    v_add.im = v1.im + v2.im;
    return v_add;
}

complex_t
complex_mpy(complex_t v1, complex_t v2) {
    complex_t v_mpy;
    v_mpy.re = (v1.re*v2.re) - (v1.im*v2.im);
    v_mpy.im = (v1.re*v2.im) + (v1.im*v2.re);
    return v_mpy;
}
