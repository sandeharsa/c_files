/* A simple program that converts Celcius degrees to Farenheit degrees
    - 9/8/2012: Modified to convert distance (mile and kg) and weight(pounds
                and kg)
 written by Sande Harsa 2/08/2012 */

#include <stdio.h>
#include <stdlib.h>

#define FAR_CONS 32
#define FAR_CONV (5.0/9.0)
#define MILETOKM 1.609
#define PTOKG 0.454

#define EXIT_FAILURE 1

int
main(int argc, char **argv) {
    
    /* Variables */
    float val, con_val;
    char unit;
    
    /* Getting input(s) */
    printf("Enter a quantity: ");
    scanf("%f%c", &val, &unit);
    /* printf("Quantity: %.2f, Unit: %c\n", val, unit); */
    
    /* Calculation and printing */
        /* Temperatre */
    if (unit == 'F' || unit == 'f') {
        con_val = val - FAR_CONS;
        con_val = con_val * FAR_CONV;
        printf("The temperature %.1f farenheit converts to %.1f celcius\n", val, con_val);
    } else if (unit == 'C' || unit == 'c') {
        con_val = val * (1/FAR_CONV);
        con_val = con_val + FAR_CONS;
        printf("The temperature %.1f celcius converts to %.1f farenheit\n", val,con_val);
        /* Distance */
    } else if (unit == 'M' || unit == 'm') {
        con_val = val * MILETOKM;
        printf("The distance %.1f miles converts to %.1f kilometers\n", val, con_val);
    } else if (unit == 'K' || unit == 'k') {
        con_val = val / MILETOKM;
        printf("The distance %.1f kilometers converts to %.1f miles\n", val,con_val);
        /* Weight */
    } else if (unit == 'P' || unit == 'p') {
        con_val = val * PTOKG;
        printf("The weight %.1f pounds converts to %.1f kilograms\n",val,con_val);
    } else if (unit == 'G' || unit == 'g') {
        con_val = val / PTOKG;
        printf("The weight %.1f kilograms converts to %.1f pounds\n",val, con_val);
    } else {
        printf("Please enter a valid value.\n");
        printf("Enter a number with either C,F,M,K,P, or G following it.\n");
        exit(EXIT_FAILURE);
    }
    
    return 0;
}
