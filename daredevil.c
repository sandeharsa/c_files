/*
//  daredevil.c
//  Solution to engineering computation workshop 12 exercise.
//
//  Created by Sande Harsa on 10/25/12.
//
*/

#include <stdio.h>
#include <math.h>

#define G 9.81
#define DELTA_T 1

double exact_calculation(double, double);
double real_calculation(double, double, double, double, double);

int
main(int argc, char **argv) {
    double h, vx;
    printf("Enter values: (h,vx): ");
    scanf("%lf,%lf", &h, &vx);
    printf("d = %f\n", exact_calculation(h,vx));
    return 0;
}

double
exact_calculation(double h, double  vx) {
    double t, d;
    t = sqrt(-2*h*(-G));
    d = vx*t;
    return d;
}

double

