/*
//  daredevil.c
//  Solution to engineering computation workshop 12 exercise.
//
//  Created by Sande Harsa on 10/25/12.
//
*/

#include <stdio.h>
#include <math.h>

#define G 9.81
#define EPSILON 1e-08
#define K 0.6 /* kg/m^2 */

typedef struct {
    double x, y;
} vector_t;

double exact_calculation(double, double);
double real_calculation(double, double, double, double, double);
double vector_magnitude(vector_t);

int
main(int argc, char **argv) {
    double h, vx;
    printf("Enter values: (h,vx): ");
    scanf("%lf,%lf", &h, &vx);
    printf("d = %f\n", exact_calculation(h,vx));
    return 0;
}

double
exact_calculation(double h, double  vx) {
    double t, d;
    t = sqrt(-2*h*(-G));
    d = vx*t;
    return d;
}

double
real_calculation(double h, double vx, double delta_t, double m, double A) {
    double Fg, Ff, magF, magV;
    Fg = m*G;
    vector_t P, V, F;
    P.y = h;
    V.x = vx;
    V.y = 0;
    while(P.y>EPSILON) {
        magF = K*A*(vector_magnitude(V))^2;
        
        
    }
}

double
vector_magnitude(vector_t v) {
    return sqrt(v.x^2 + v.y^2)
}
