We have all used search services to locate web pages with desired
information.
In response to a query, a search engine result page (known as the
SERP) is generated, containing typically ten suggestion, each of
which consists of a URL, and a snippet, a short extract from the
prose of the corresponding page or document.
A typical snippet contains around 20-30 words and is selected from
the corresponding document to show the context in which all (if
possible) or some (if all is not possible) of the query words appear.
In early search services the snippets were generated for the document
at the time it was indexed, and were independent of the particular
query.
Query biased snippets involve rather more computation and add to the
time taken to evaluate queries, but are generally agreed to be more
intuitive for the user to digest and react to.
========
We have all used search services to locate web pages with desired
information.
In response to a query, a search engine result page (known as the
SERP) is generated, containing typically ten suggestion, each of
which consists of a URL, and a snippet, a short extract from the
prose of the corresponding page or document.
A typical snippet contains around 20-30 words and is selected from
the corresponding document to show the context in which all (if
possible) or some (if all is not possible) of the query words appear.
In early search services the snippets were generated for the document
at the time it was indexed, and were independent of the particular
query.
Query biased snippets involve rather more computation and add to the
time taken to evaluate queries, but are generally agreed to be more
intuitive for the user to digest and react to.
========
We have all used search services to locate web pages with desired
information.
In response to a query, a search engine result page (known as the
SERP) is generated, containing typically ten suggestion, each of
which consists of a URL, and a snippet, a short extract from the
prose of the corresponding page or document.
A typical snippet contains around 20-30 words and is selected from
the corresponding document to show the context in which all (if
possible) or some (if all is not possible) of the query words appear.
In early search services the snippets were generated for the document
at the time it was indexed, and were independent of the particular
query.
Query biased snippets involve rather more computation and add to the
time taken to evaluate queries, but are generally agreed to be more
intuitive for the user to digest and react to.
========
