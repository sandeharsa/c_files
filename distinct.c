/*
//  distinct.c
//
// 
//
//  Created by Sande Harsa on 8/13/12.
//
*/

#include <stdio.h>

#define MAXVALS 1000

int read_int_array(int[], int);
void sort_int_array(int[], int);
void print_int_array(int[], int);
void int_swap(int*, int*);
int distinct(int[], int[], int);

int
main(int argc, char **argv) {
	int numbers[MAXVALS], num_dist[MAXVALS], valid, val_dist;
	valid = read_int_array(numbers, MAXVALS);
	printf("Before  : ");
	print_int_array(numbers, valid);
	sort_int_array(numbers, valid);
	printf("After   : ");
    print_int_array(numbers, valid);
    val_dist = distinct(numbers, num_dist, valid);
    printf("Distinct: ");
	print_int_array(num_dist, val_dist);
	return 0;
}

int
read_int_array(int A[], int maxvals) {
	int n, excess, next;
	printf("Enter as many as %d values, ^D to end\n",
           maxvals);
	n = 0; excess = 0;
	while (scanf("%d", &next)==1) {
		if (n==maxvals) {
			excess = excess+1;
		} else {
			A[n] = next;
			n += 1;
		}
	}
	printf("%d values read into array", n);
	if (excess) {
		printf(", %d excess values discarded", excess);
	}
	printf("\n");
	return n;
}

void
sort_int_array(int A[], int n) {
	int i, didswaps;
	/* assume that A[0] to A[n-1] have valid values */
	didswaps = 1;
	while (didswaps) {
		didswaps = 0;
		for (i=0; i<n-1; i++) {
			if (A[i] > A[i+1]) {
				int_swap(&A[i], &A[i+1]);
				didswaps = 1;
			}
		}
	}
}

void
print_int_array(int A[], int n) {
	int i;
	for (i=0; i<n; i++) {
		printf(" %d", A[i]);
	}
	printf("\n");
}

int
distinct(int A[], int B[], int n) {
    int i, j;
    for (i=0, j=0; i<n; i++) {
        if (A[i] != A[i-1]) {
            B[j] = A[i];
            j++;
        }
    }
    return j;
}

/* exchange the values of the two variables indicated
 by the arguments */
void
int_swap(int *x, int *y) {
	int t;
	t = *x;
	*x = *y;
	*y = t;
}
