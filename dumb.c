/*
//  dumb.c
//  Dumb test
//
//  Created by Sande Harsa on 8/30/12.
//
*/

#include <stdio.h>

int dumb(int);
int dumber(int);


int
main(int argc, char **argv) {
    int num;
    printf("Enter dumb test number: ");
    scanf("%d", &num);
    printf("Dumb = %d\n", dumb(num));
    printf("Dumber = %d\n", dumber(num));
}

int
dumb(int dumb) {
    return dumb+1;
}

int
dumber(int dumber) {
    /*int dumber=6;*/
    return dumb(dumber+1);
}