/*
//  freqcnt.c
//  Count the frequency of each value in an array and print it in a 2D format.
//
//  Created by Sande Harsa on 8/13/12.
//
*/

#include <stdio.h>

#define MAXVALS 1000

int read_int_array(int[], int);
int distinct(int[], int[], int);
void sort_int_array(int[], int);
void print_int_array(int[], int);
void print_int_array2(int[], int[], int);
void int_swap(int*, int*);
void freq_int_array(int[], int[], int[], int, int);

int
main(int argc, char **argv) {
	int numbers[MAXVALS], num_dist[MAXVALS], valid, val_dist;
	valid = read_int_array(numbers, MAXVALS);
	sort_int_array(numbers, valid);
    
    val_dist = distinct(numbers, num_dist, valid);
    int counter[MAXVALS] = {0};
    
    freq_int_array(numbers, num_dist, counter, valid, val_dist);
    printf("Value     Freq\n");
	print_int_array2(num_dist, counter, val_dist);
	return 0;
}

int
read_int_array(int A[], int maxvals) {
	int n, excess, next;
	printf("Enter as many as %d values, ^D to end\n",
           maxvals);
	n = 0; excess = 0;
	while (scanf("%d", &next)==1) {
		if (n==maxvals) {
			excess = excess+1;
		} else {
			A[n] = next;
			n += 1;
		}
	}
	printf("%d values read into array", n);
	if (excess) {
		printf(", %d excess values discarded", excess);
	}
	printf("\n");
	return n;
}

void
sort_int_array(int A[], int n) {
	int i, didswaps;
	/* assume that A[0] to A[n-1] have valid values */
	didswaps = 1;
	while (didswaps) {
		didswaps = 0;
		for (i=0; i<n-1; i++) {
			if (A[i] > A[i+1]) {
				int_swap(&A[i], &A[i+1]);
				didswaps = 1;
			}
		}
	}
}

void
print_int_array(int A[], int n) {
	int i;
	for (i=0; i<n; i++) {
		printf(" %d", A[i]);
	}
	printf("\n");
}

void
print_int_array2(int A[], int B[], int n) {
    int i;
	for (i=0; i<n; i++) {
		printf("%4d     %4d\n", A[i], B[i]);
	}
}

int
distinct(int A[], int B[], int n) {
    int i, j;
    for (i=0, j=0; i<n; i++) {
        if (A[i] != A[i-1]) {
            B[j] = A[i];
            j++;
        }
    }
    return j;
}

void
freq_int_array(int A[], int B[], int C[], int n, int p) {
    int i, j, counter;
    for (i=0; i<p; i++) {
        counter = 0;
        for (j=0; j<n; j++) {
            if (B[i] == A[j]) {
                C[i]++;
            }
        }
    }
}

/* exchange the values of the two variables indicated
 by the arguments */
void
int_swap(int *x, int *y) {
	int t;
	t = *x;
	*x = *y;
	*y = t;
}
