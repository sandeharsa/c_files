#include <stdio.h>

int
main(int argc, char **argv) {
    int x, i;
    
    printf("Enter numbers:");
    while(1) {
        scanf("%d", &x);
        if ((x<1) || (x>70)) {
            printf("Please input a value from 1-70:\n");
        } else {
        printf("%-2d |", x);
        for(i=0; i<x; i++) {
            printf("*");
        }
        printf("\n");
        }
    }
    
    return 0;
}
