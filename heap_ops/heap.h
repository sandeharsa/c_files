/* heap.h
	Header file for heap operations.
*/

/* Element (item) of the heap */
typedef struct item {
	float key; // the key for deciding position in heap
	uint dataIndex; // the payload index provided by the calling program
} HeapItem;

typedef struct heap {
	HeapItem *H; // the underlying array
	uint *map; // map[i] is index into H of location of payload with dataIndex == i
	uint n; // the number of items currently in the heap
	uint size; // the maximum number of items allowed in the heap
} Heap;

