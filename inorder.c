/* inorder.c
   Made for q2 from the eng comp sample test.


*/


#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char **argv) {
    int num, next;
    scanf("%d", &num);
    while(scanf("%d", &next) == 1) {
        if (next<num) {
            printf("*** value %d is smaller than %d\n", next, num);
        }
        num = next;
    }


    return 0;
}