/*
//  insertion_sort.c
//  implementation of recursive insertion sort.
//
//  Created by Sande Harsa on 10/30/12.
//
*/

#include <stdio.h>
#include "read_int_array.h"

#define MAX_VALUES 1000

void print_int_array(int[], int);
void insertionsort(int[], int);
void int_swap(int[], int);

int
main(int argc, char** argv) {
    int A[MAX_VALUES], limit;
    limit = read_int_array(A, MAX_VALUES);
    insertionsort(A, limit);
    print_int_array(A, limit);
    return 0;
}

void
insertionsort(int A[], int n) {
    /* base case */
    if(n<=1) {
        return;
    }
    /* recursive case */
    insertionsort(A, n-1);
    int_swap(A, n);
    return;
}

void int_swap(int A[], int n) {
    int i, temp;
    for(i=n;i>0;i--) {
        if(A[i]<A[i-1]) {
            temp = A[i];
            A[i] = A[i-1];
            A[i-1] = temp;
        } else break;
    }
}

void
print_int_array(int A[], int n) {
    int i;
    for(i=0;i<n;i++) {
        printf("%3d", A[i]);
    }
    printf("\n");
}


