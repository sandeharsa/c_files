/*
//  lin_insertion.c
//  Sorting with the linear insertion sort algorithm.
//
//  Created by Sande Harsa on 10/4/12.
//
*/

#include <stdio.h>
#include <string.h>

#define MAX_ARRAY_LEN 300

void int_sort(int[], int);
void insert(int[], int, int, int);
int read_int_array(int[], int);
void print_int_array(int[], int);

int
main (int argc, char **argv) {
    int valid;
    int nums[MAX_ARRAY_LEN];
    valid = read_int_array(nums, MAX_ARRAY_LEN);
    printf("Before : ");
    print_int_array(nums, valid);
    int_sort(nums, valid);
    printf("After  : ");
    print_int_array(nums, valid);
    return 0;
}

void
int_sort(int *s, int valid) {
    int i, j, pos, s_ed[MAX_ARRAY_LEN];
    memset(s_ed, 0, MAX_ARRAY_LEN*sizeof(int));
    for (i=0;i<valid;i++) {
        pos = 0;
        if (i==0) {
            s_ed[i] = s[i];
        } else {
            j=i-1;
            while (j>=0 && s[i]<s[j]) {
                j--;
            }
            if(j==-1) {
                j=0;
            }
            insert(s_ed, j, s[i], valid);
        }
    }
    for (i=0;i<valid;i++) {
        s[i] = s_ed[i];
    }
}

void
insert(int s[], int pos, int num, int end) {
    int i;
    if(s[pos]==0) {
        s[pos] = num;
    } else {
        for(i=end-1;i>=pos;i--) {
            s[i+1] = s[i];
        }
        s[pos] = num;
    }
}

int
read_int_array(int A[], int maxvals) {
	int n, excess, next;
	printf("Enter as many as %d values, ^D to end\n",
           maxvals);
	n = 0; excess = 0;
	while (scanf("%d", &next)==1) {
		if (n==maxvals) {
			excess = excess+1;
		} else {
			A[n] = next;
			n += 1;
		}
	}
	printf("%d values read into array", n);
	if (excess) {
		printf(", %d excess values discarded", excess);
	}
	printf("\n");
	return n;
}

void
print_int_array(int A[], int n) {
	int i;
	for (i=0; i<n; i++) {
		printf("%3d", A[i]);
	}
	printf("\n");
}

