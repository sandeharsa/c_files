/* Example program to illustrate linked list operations.
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int data_t;
#include "listops.c"

int
main(int argc, char **argv) {
	list_t *list;
	int i;
    int A[]={5,3,7,4,2,6,1,8,9};
	list = make_empty_list(compare_int);
	for(i=0;i<9;i++) {
        list = insert_in_order(list, A[i]);
    }
	while (!is_empty_list(list)) {
		i = get_head(list);
		printf("%d ", i);
		list = get_tail(list);
	}
	printf("\n");
	free_list(list);
	list = NULL;
	return 0;
}

