typedef struct node node_t;

struct node {
	data_t data;
	node_t *next;
};

typedef struct {
	node_t *head;
	node_t *foot;
    int (*cmp)(void*,void*);
} list_t;

list_t
*make_empty_list(int cmp(void*,void*)) {
	list_t *list;
	list = malloc(sizeof(*list));
	assert(list!=NULL);
	list->head = list->foot = NULL;
    list->cmp = cmp;
	return list;
}

int
is_empty_list(list_t *list) {
	assert(list!=NULL);
	return list->head==NULL;
}

void
free_list(list_t *list) {
	node_t *curr, *prev;
	assert(list!=NULL);
	curr = list->head;
	while (curr) {
		prev = curr;
		curr = curr->next;
		free(prev);
	}
	free(list);
}

list_t
*insert_at_head(list_t *list, data_t value) {
	node_t *new;
	new = malloc(sizeof(*new));
	assert(list!=NULL && new!=NULL);
	new->data = value;
	new->next = list->head;
	list->head = new;
	if (list->foot==NULL) {
		/* this is the first insertion into the list */
		list->foot = new;
	}
	return list;
}

list_t
*insert_at_foot(list_t *list, data_t value) {
	node_t *new;
	new = malloc(sizeof(*new));
	assert(list!=NULL && new!=NULL);
	new->data = value;
	new->next = NULL;
	if (list->foot==NULL) {
		/* this is the first insertion into the list */
		list->head = list->foot = new;
	} else {
		list->foot->next = new;
		list->foot = new;
	}
	return list;
}

list_t
*insert_in_order(list_t *list, data_t value) {
    /* construct the new node */
    node_t *new, *current, *nxt, *last;
    new = malloc(sizeof(*new));
    assert(list!=NULL && new!=NULL);
    new->data = value;
    new->next = NULL;
    /* compare the value to find the appropriate position */
    if (list->head == NULL) {
        list->head = new;
        list->foot = new;
        return list;
    }
    nxt = list->head;
    current = list->head;
    while (nxt) {
        if (list->cmp(&new->data,&nxt->data)<=0) {
            if (list->head == nxt) {
                list->head = new;
            } else {
                current->next = new;
            }
            new->next = nxt;
            return list;
        }
        current = nxt;
        nxt = nxt->next;
    }
    /* if the data haven't been inserted, it belongs to the foot */
    last = list->foot;
    last->next = new;
    list->foot = new;
    return list;
}

data_t
get_head(list_t *list) {
	assert(list!=NULL && list->head!=NULL);
	return list->head->data;
}

list_t
*get_tail(list_t *list) {
	node_t *oldhead;
	assert(list!=NULL && list->head!=NULL);
	oldhead = list->head;
	list->head = list->head->next;
	if (list->head==NULL) {
		/* the only list node just got deleted */
		list->foot = NULL;
	}
	free(oldhead);
	return list;
}

int
compare_int(void *x, void *y) {
    int *i = x, *j = y;
    if (*i < *j) return -1;
    else if (*i > *j) return 1;
    else return 0;
}

/* =====================================================================
   Program written by Alistair Moffat, as an example for the book
   "Programming, Problem Solving, and Abstraction with C", Pearson
   SprintPrint, Sydney, Australia, 2003.

   See http://www.csse.unimelb.edu.au/~alistair/ppsaa/ for further
   information.

   This version prepared January 2012.
 
   Modified by sharsa (6/10/12):
        - New function: insert_in_order
   ================================================================== */
