/*
//  log.c
//  
//
//  Created by Sande Harsa on 8/9/12.
//
*/

#include <stdio.h>

int
main(int argc, char **argv) {
    double x;
    int cycles = 0;
    
    printf("Enter starting value for x: ");
    scanf("%lf", &x);
    
    while(x > 1) {
        printf("%.0f ", x);
        cycles++;
        x = x / 2;
    }
    
    printf("    log_2 = %d\n", cycles);
    
    return 0;
}