/*
//  matrixops.c
//  Matrix operations (square matrices).
//
//  Created by Sande Harsa on 10/29/12.
//
*/

#include <stdio.h>
#include <stdlib.h>

#define COLS 3
#define ROWS 3

typedef int data_t;
typedef data_t matrix_t[ROWS][COLS];

void transpose(matrix_t);
void multiplication(matrix_t, matrix_t, matrix_t);
void load_matrix(matrix_t, int, char**);
void print_matrix(matrix_t);

int
main(int argc, char **argv) {
    matrix_t M = {{1,2,3},{2,4,6},{3,6,9}};
    matrix_t Ans;
    load_matrix(M, argc, argv);
    printf("Original matrix (M):\n");
    print_matrix(M);
    printf("\n");
    printf("Transpose (M'):\n");
    transpose(M);
    print_matrix(M);
    printf("\n");
    printf("Multiplication (M*M):\n");
    multiplication(M, M, Ans);
    print_matrix(Ans);
    
    return 0;
}

void
transpose(matrix_t M) {
    int i, j;
    data_t temp;
    for(i=0;i<ROWS;i++) {
        for(j=i;j<COLS;j++) {
            temp = M[i][j];
            M[i][j] = M[j][i];
            M[j][i] = temp;
        }
    }
}

void
multiplication(matrix_t M1, matrix_t M2, matrix_t Ans) {
    int i, j, k;
    for(i=0;i<ROWS;i++) {
        for(j=0;j<COLS;j++) {
            Ans[i][j] = 0;
            for(k=0;k<ROWS;k++) {
                Ans[i][j] += (M1[i][k]*M2[k][j]);
            }
        }
    }
}

void
load_matrix(matrix_t M, int argc, char **argv) {
    int i, j, n;
    if(argc != ROWS*COLS+1) {
        printf("Usage: ./matrixops <%d values>\n", ROWS*COLS);
        exit(EXIT_FAILURE);
    }
    n=1;
    for(i=0;i<ROWS;i++) {
        for(j=0;j<COLS;j++) {
            M[i][j] = (data_t)atof(argv[n]);
            n++;
        }
    }
}

void
print_matrix(matrix_t M) {
    int i, j;
    for(i=0;i<ROWS;i++) {
        for(j=0;j<COLS;j++) {
            printf("%5d", M[i][j]);
        }
        printf("\n");
    }
}
