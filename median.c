/*
//  median.c
//  
//
//  Created by Sande Harsa on 8/8/12.
//
*/

#include <stdio.h>

int
main(int argc, char **argv) {
    float num1, num2, num3, max, min, med;
    
    /* Input */
    printf("Please enter 3 numbers:");
    scanf("%f%f%f", &num1, &num2, &num3);
    
    med = num1;
    max = num1;
    min = num1;
    
    /* Search for the max value */
    if ((num2 > num1) && (num2 > num3)) {
        max = num2;
    } else if ((num3 > num1) && (num3 > num2)) {
        max = num3;
    } else {
        ;
    }
    
    printf("Max: %f\n", max);
    
    /* Search for the min value */
    if ((num2 < num1) && (num2 < num3)) {
        min = num2;
    } else if ((num3 < num1) && (num3 < num2)) {
        min = num3;
    } else {
        ;
    }
    
    printf("Min: %f\n", min);
    
    /* Search for the median */
    if ((num1 == max) && (num2 == min)) {
        med = num3;
    } else if((num1 == min) && (num2 == max)) {
        med = num3;
    } else if((num1 == min) && (num3 == max)) {
        med = num2;
    } else if ((num1 == max) && (num3 == min)) {
        med = num2;
    } else {
        ;
    }
    
    /* Prnt out the value */
    printf("The median value is %f\n", med);
    
    return 0;
    
}