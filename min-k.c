#include <stdio.h>
#include <stdlib.h>

#define MAXVALS 1000

int mink(int[], int, int);
int read_int_array(int[], int);
void int_swap(int*, int*);


int
main(int argc, char **argv) {
    int k, valid, numbers[MAXVALS];

    printf ("Please enter the k number:\n");
    scanf ("%d", &k);
    valid = read_int_array(numbers, MAXVALS);
    if (valid<k) {
        printf("Please enter at least k (%d) number(s)\n", k);
        exit(EXIT_FAILURE);
    }
    printf ("The %dth smallest number is: %d", k, mink(numbers, k, valid));
    
    return 0;
 }
 

int
read_int_array(int A[], int maxvals) {
	int n, excess, next;
	printf("Enter as many as %d values, ^D to end\n",
           maxvals);
	n = 0; excess = 0;
	while (scanf("%d", &next)==1) {
		if (n==maxvals) {
			excess = excess+1;
		} else {
			A[n] = next;
			n += 1;
		}
	}
	printf("%d values read into array", n);
	if (excess) {
		printf(", %d excess values discarded", excess);
	}
	printf("\n");
	return n;
}

 
int
mink (int A[], int k, int n) {
    int i, j, min;
    for (i=0;i<k-1;i++) {
        min = A[0];
    		for (j=0;j<n;j++) {
       		 if (A[i] < min) {
                    min = A[i];
                }
        }       
        int_swap(&min, &A[i]);
    }
    return A[k-1];
}
    

 /* exchange the values of the two variables indicated
 by the arguments */
void
int_swap(int *x, int *y) {
	int t;
	t = *x;
	*x = *y;
	*y = t; 
}
