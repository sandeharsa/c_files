/*
//  myislower.c
//  A version of the islower() function which checks whether a char is
//  lowercase or not.
//  Created by Sande Harsa on 8/14/12.
//
*/

#define ANSI_UA 65
#define ANSI_UZ 90
#define ANSI_LA 97
#define ANSI_LZ 122

#include <stdio.h>

int myislower(int);

int
main(int argc, char **argv) {
    int ch;
    printf("Enter a letter:\n");
    ch = getchar();
    
    if (myislower(ch) == 1) {
        printf("True\n");
    } else if (myislower(ch) == 0) {
        printf("False\n");
    } else {
        printf("The input was not a letter.\n");
    }
    
    
    return 0;
}

int myislower(int ch) {
    if ((ANSI_UA <= ch) && (ch <= ANSI_UZ)) {
        return 0;
    } else if ((ANSI_LA <= ch) && (ch <= ANSI_LZ)) {
        return 1;
    } else {
        return 2;
    }
}