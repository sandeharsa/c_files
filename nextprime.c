/*
//  nextprime.c
//  
//
//  Created by Sande Harsa on 8/16/12.
//
*/

#include <stdio.h>
#include <stdlib.h>

int nextprime(int);

int
main(int argc, char **argv) {
    int num;
    printf("Enter an integer value: ");
    scanf("%d\n", &num);
    if (num<0) {
        num=0;
    }
    printf("The next prime is     : %d\n", nextprime(num));
    
    return 0;
}





int
nextprime(int num) {
    int i,prime;
    num++;
    if (num==1) {
        num++;
    }
    for (;;num++) {
        prime=1;
        for (i=2;i<num;i++) {
            if (num%i == 0) {
                prime=0;
                break;
            }
        }
        if (prime) {
            return num;
        }
    }
    return num;
}