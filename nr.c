/*
//  nr.c
//  Root finding by Newton-Raphson approach. Exercise for COMP20005.
//
//  Created by Sande Harsa on 10/29/12.
//
*/

#include <stdio.h>
#include <math.h>

#define TRESH 1e-01 

typedef double number;

number f(number);
number derf(number);
number root_finding(number);

int
main(int argc, char **argv) {
    number i;
    for(i=TRESH;i>(1.0e-15);i/=100.0) {
        printf("root = %.8f\n", root_finding(i));
    }
    return 0;
}

number
f(number x) {
    return x*x - 10;
}

number
derf(number x) {
    return 2*x;
}

number
nr(number x) {
    return x - (f(x)/derf(x));
}

number
root_finding(number tresh) {
    double x=1;
    while(fabs(f(x)) > tresh) {
        x = nr(x);
    }
    return x;
}



