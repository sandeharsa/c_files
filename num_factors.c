#include <stdio.h>

int num_factor(int);

int
main(int argc, char **argv) {
    int n;

    printf("Enter a number: ");
    scanf("%d", &n);
    printf("Number %d has %d factors.", n, num_factor(n));
    
    
    
    return 0;
}

int
num_factor(int n) {
    int i, facs=0;
    for(i=1;i<=n;i++) {
        if(n%i == 0) {
            facs += 1;
        }
    }
    
    return facs;
}
