/*
//  palindrome.c
//  Checks whether a astring is a palindrome or not.
//
//  Created by Sande Harsa on 10/3/12.
//
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_STR_LEN 1000

int is_palindrome(char*);

int
main(int argc, char **argv) {
    char s[MAX_STR_LEN];
    printf("Input string to test:\n");
    while(gets(s)) {
        if (is_palindrome(s)) {
            printf("The string is a palindrome.\n");
        } else {
            printf("The string is not a palindrome.\n");
        }
    }
    return 0;
}

int
is_palindrome(char *s) {
    int i, j;
    /* iterate forwards (the string itself) and backwards (reversed string) */
    for (i=0,j=(strlen(s)-1); i<strlen(s) && j>=0;i++,j--) {
        /* ignore non-alphanumeric chars */
        while (!isalnum(s[i])) {
            i++;
        }
        while (!isalnum(s[j])) {
            j--;
        }
        /* compare each letter, ignoring the case */
        if (tolower(s[i])!=tolower(s[j])) {
            return 0;
        }
    }
    
    return 1;
}
