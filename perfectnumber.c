/*
//  perfectnumber.c
//    Determine if a number is a perfect number (a number that is equal to the
//  sum of the factors of the number).
//  Created by Sande Harsa on 8/22/12.
//
*/

#include <stdio.h>

#define IS_PERFECT 1
#define NOT_PERFECT 0

int isperfect(int);
int nextperfect(int);


int
main(int argc, char **argv) {
    int num;
    printf("Enter a number:\n");
    scanf("%d", &num);
    
    if(isperfect(num)) {
        printf("The number %d is a perfect number\n", num);
    } else {
        printf("The number %d is NOT a perfect number\n", num);
    }
    
    printf("The next perfect number is: %d\n", nextperfect(num));

    return 0;
}
/Volumes/DATA/Users/sandeharsa/Documents/[Studies]/UniMelb/S2 - Foundation of Algorithms/C_Files/perfectnumber.c
int
isperfect(int num) {
    int sum=1, i;
    for(i=2;i*i<num;i++) {
        if (num%i == 0) {
            sum += i;
            sum += num/i;
        }
    }
    
    if (sum == num) {
        return IS_PERFECT;
    }
    
    return NOT_PERFECT;
}

int
nextperfect(int num) {
    int i;
    for(i=num+1;!(isperfect(i));i++);
    return i;
}