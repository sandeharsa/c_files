/*
//  plus.c
//  Practice program for algo test.
//
//  Created by Sande Harsa on 8/25/12.
//
*/

#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int
main(int argc, char **argv) {
    int i, j, num;
    printf("Enter a value between 1 and 10: ");
    if (scanf("%d", &num) != 1) {
        printf("Invalid input\n");
        exit(EXIT_FAILURE);
    } else if ((num<1) || (num>10)) {
        printf("Must be between 1 and 10\n");
        exit(EXIT_FAILURE);
    }
    
    for (i=0;i<(num*2)-1;i++) {
        if (i==num-1) {
            for (j=0;j<((num*2)-1);j++) {
                printf("+ ");
            }
            printf("\n");
        } else {
            for (j=0;j<(num-1)*2;j++) {
                printf(" ");
            }
            printf("+");
            printf("\n");
        }
    }
    
    return 0;
}