/* Calculate a quadratic formula */

#include <stdio.h>
#include <math.h>
int
main(int argc, char **argv) {
    float a=0.0, b=0.0, c=0.0, d=0.0, sol1=0.0, sol2=0.0;
    
    printf("Enter the value of a, b, and c:\n");
    scanf("%f%f%f", &a, &b, &c);
    printf("a = %.2f\nb = %.2f\nc = %.2f\n", a, b, c);
    d = (b*b) - (4*a*c);
    printf("Discriminant: %f\n", d);
    
    if (b == 0.0) {
        if (a > 0 && c > 0) {
            printf("There is no real roots since both a and c are positive");
            return 1;
        }
    }
    
    if (d<0) {
        printf("There are no real roots.\n");
        return 1;
    }
    
    sol1 = ((-b) + sqrt(d))/2*a;
    sol2 = ((-b) - sqrt(d))/2*a;
    
    if (d == 0) {
        printf("The quadratic form only has one answer since d = 0\n");
        printf("Answer:\n");
        printf("- Root 1: %.2f\n", sol1);
    } else {
        printf("Answer:\n");
        printf("- Root 1: %.2f\n", sol1);
        printf("- Root 2: %.2f\n", sol2);
    }
    
    
    return 0;
}