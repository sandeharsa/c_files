#include <stdio.h>
#include <string.h>

char* reverse(char*, int);

int
main(int argc, char **argv) {
    char a[17]="abcdefg";
    printf("%s\n", reverse(a,strlen(a)));
    
    return 0;
}

char* reverse(char *a, int len) {
    char temp;
    if(strlen(a)==1) {
        return a;
    } else {
        temp = *a;
        a = reverse(a+1, len-1);
    }
}