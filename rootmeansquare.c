/*
//  rootmeansquare.c
//  Second question of the sample mid semester test.
//
//  Created by Sande Harsa on 8/25/12.
//
*/

#include <stdio.h>
#include <math.h>

#include "read_int_array.h"

#define MAXVALS 100
#define RMS_INVALID 0
#define RMS_VALID 1

int calc_rms(double[], double[], int, double*);

int
main(int argc, char **argv) {
    int valid;
    double X[MAXVALS], Y[MAXVALS], *rms;
    printf("Enter the first array:\n");
    valid = read_int_array(X, MAXVALS);
    printf("Enter the second array:\n");
    valid = read_int_array(Y, MAXVALS);
    
    calc_rms(X, Y, valid, rms);
    printf("RMS = %f\n", *rms);
    
    return 0;
}

int
calc_rms(double X[], double Y[], int n, double *rms) {
    int i;
    double sum;
    if (n<=0) {
        return RMS_INVALID;
    } else {
        for (i=0;i<n;i++) {
            sum += pow((X[i]-Y[i]),2);
        }
        *rms = sqrt(sum/n);
        return RMS_VALID;
    }
}