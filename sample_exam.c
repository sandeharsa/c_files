/*
//  sample_exam.c
//  Answers to sample exam Foundation of Algorithm 2012.
//
//  Created by Sande Harsa on 11/18/12.
//
*/

#include <stdio.h>

#define MAXSEQLEN 5000
#define MAXPOINTS 1000000
#define ONE_TENTH_SECOND (1.0/24.0/60.0/60.0/10.0)

typedef struct {
    double vals[MAXSEQLEN];
    int nvals;
} seq_t;

typedef struct {
    int yy, mh, dd;
    int hh, mn, ss;
} datetime_t;

typedef struct {
    double lat, lon;
    double altitude;
    double timestamp1;
    datetime_t timestamp2;
} gpsrecord_t;

double sum_sequence(int);
double seq_mean(seq_t);
void calc_trend(seq_t *s, seq_t *t);

int
main(int argc, char **argv) {
    int i;
    seq_t test_seq, t;
    test_seq.vals[0] = 1.0;
    test_seq.vals[1] = 1.5;
    test_seq.vals[2] = -1.5;
    test_seq.vals[3] = -1.0;
    test_seq.vals[4] = 3.2;
    test_seq.vals[5] = 3.4;
    test_seq.vals[6] = 3.4;
    test_seq.nvals = 7;
    /* Test Q1 */
    printf("Test Question 1:\n\tsum_sequence(3) = %.1f\n", sum_sequence(3));\
    /* Test Q2 */
    printf("Test Question 2:\n\ttest_seq: ");
    for(i=0;i<test_seq.nvals;i++) {
        printf("%.0f ", test_seq.vals[i]);
    }
    printf("\n");
    printf("\tb. seq_mean(test_seq) = %.1f\n",seq_mean(test_seq));
    printf("\tc. trend: ");
    calc_trend(&test_seq, &t);
    for(i=0;i<t.nvals;i++) {
        printf("%.0f ", t.vals[i]);
    }
    printf("\n");
    return 0;
}

/* Question 1 */
double
sum_sequence(int n) {
    int i;
    double sum=0, num=0, div=1;
    for(i=1;i<=n;i++) {
        num += i;
        div *= i;
        sum += num/div;
    }
    return sum;
}
    /* This program will take O(n) execution time */
    /* There might be a problem with large n since adition of many numbers
     could rise rounding errors */

/* Question 2 */
    /* a. The purpose of nvals is to store the number of values currently
     stored in the arraay vals */
    /* b. double seq_mean(seq_t s) (below) */
double
seq_mean(seq_t s) {
    int i;
    double sum=0;
    for(i=0;i<s.nvals;i++) {
        sum += i;
    }
    return sum/s.nvals;
}
    /* c. void calc_trend(seq_t *s, seq_t *t) (below) */
void
calc_trend(seq_t *s, seq_t *t) {
    int i;
    double cur;
    for(i=0;i<(s->nvals-1);i++) {
        cur = s->vals[i+1] - s->vals[i];
        if(cur<0) t->vals[i] = -1;
        else if(cur>0) t->vals[i] = 1;
        else t->vals[i] = 0;
    }
    t->nvals=i;
}

    /* d. trend_correl(seq_t *pm seq_t *q) (below) */
double
trend_correl(seq_t *p, seq_t *q) {
    seq_t tp, tq;
    int i;
    double sum;
    calc_trend(p, &tp);
    calc_trend(q, &tq);
    for(i=0;i<(p->nvals);i++) {
        sum += tp[i] * tq[i];
    }
    return sum/i;
}

/* Question 3 */
    /* a. (below) */
int
gps_compare(gpsrecord_t *g1, gpsrecord_t *g2) {
    if(g1->timestamp1 > g2->timestamp1) {
        return 1;
    } else if(g1->timestamp1 < g2->timestamp1) {
        return -1;
    } else {
        return 0;
    }
}

static int recursive_tree_size(node_t *root);

static int
recursive_tree_size(node_t *n) {
    if(!n) {
        return 0;
    } else {
        return recursive_tree_size(n->left) + recursive_tree_size(n->right) + 1;
    }
}

int
tree_size(tree_t *t) {
    assert(t!=NULL);
    return recursive_tree_size(t->root);
}





