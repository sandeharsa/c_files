/*
//  selectionsort.c
//  Sort an array using the selection sort algorithm.
//
//  Created by Sande Harsa on 8/13/12.
//
*/


#include <stdio.h>

#define MAXVALS 1000

int read_int_array(int[], int);
int distinct(int[], int[], int);
void sort_int_array(int[], int);
void print_int_array(int[], int);
void print_int_array2(int[], int[], int);
void int_swap(int*, int*);
void freq_int_array(int[], int[], int[], int, int);
void selection_sort(int[], int n);
void selection_sort1(int[], int n);
void selection_sort2(int[], int n);
void max_value(int[], int*, int);


int
main(int argc, char **argv) {
    int numbers[MAXVALS], valid;
	valid = read_int_array(numbers, MAXVALS);
	printf("Before: ");
	print_int_array(numbers, valid);
	selection_sort2(numbers, valid);
	printf("After : ");
	print_int_array(numbers, valid);
	return 0;
}

int
read_int_array(int A[], int maxvals) {
	int n, excess, next;
	printf("Enter as many as %d values, ^D to end\n",
           maxvals);
	n = 0; excess = 0;
	while (scanf("%d", &next)==1) {
		if (n==maxvals) {
			excess = excess+1;
		} else {
			A[n] = next;
			n += 1;
		}
	}
	printf("%d values read into array", n);
	if (excess) {
		printf(", %d excess values discarded", excess);
	}
	printf("\n");
	return n;
}

void
sort_int_array(int A[], int n) {
	int i, didswaps;
	/* assume that A[0] to A[n-1] have valid values */
	didswaps = 1;
	while (didswaps) {
		didswaps = 0;
		for (i=0; i<n-1; i++) {
			if (A[i] > A[i+1]) {
				int_swap(&A[i], &A[i+1]);
				didswaps = 1;
			}
		}
	}
}

/* Alternative sorting */
void
selection_sort(int A[], int n) {
    int i, j, upto, *max;
    upto = n;
    for (i=0; i<n; i++) {
        max = &A[0];
        for (j=0; j<upto; j++) {
            if (*max  < A[j]) {
                max = &A[j]; 
            }
        }
        int_swap(max, &A[upto-1]);
        upto--;
    }
}

/* Alternative sorting (Recursive) */
void
selection_sort1(int A[], int upto) {
    int *max, i;
    if (upto <= 0) {
        return;
    } else {
        max = &A[0];
        for (i=0; i<upto; i++) {
            if (*max < A[i]) {
                max = &A[i];
            }
        }
    }
    int_swap(max, &A[upto-1]);
    upto--;
    selection_sort1(A, upto);
}

/* Modifications of selectio_sort1 */
void
selection_sort2(int A[], int upto) {
    int *max;
    if (upto <= 0) {
        return;
    } else {
        max = &A[upto-1];
        if (*max < A[i]) {
            max = &A[i];
            int_swap(max, &A[upto-1]);
        }
    }
    i++;
    upto--;
    selection_sort2(A, upto);
}

void
print_int_array(int A[], int n) {
	int i;
	for (i=0; i<n; i++) {
		printf(" %d", A[i]);
	}
	printf("\n");
}

void
print_int_array2(int A[], int B[], int n) {
    int i;
	for (i=0; i<n; i++) {
		printf("%4d     %4d\n", A[i], B[i]);
	}
}

int
distinct(int A[], int B[], int n) {
    int i, j;
    for (i=0, j=0; i<n; i++) {
        if (A[i] != A[i-1]) {
            B[j] = A[i];
            j++;
        }
    }
    return j;
}

void
freq_int_array(int A[], int B[], int C[], int n, int p) {
    int i, j, counter;
    for (i=0; i<p; i++) {
        counter = 0;
        for (j=0; j<n; j++) {
            if (B[i] == A[j]) {
                C[i]++;
            }
        }
    }
}

/* exchange the values of the two variables indicated
 by the arguments */
void
int_swap(int *x, int *y) {
	int t;
	t = *x;
	*x = *y;
	*y = t;
}
