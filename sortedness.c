/*
//  sortedness.c
//  Quantifying how close an array is to being sorted.
//
//  Created by Sande Harsa on 8/20/12.
//
*/

#include <stdio.h>

int read_int_array(int[], int);
int asc_runs(int[], int);
int inversions(int[], int);

#define MAXVALS 1000

int
main(int argc, char **argv) {
    int numbers[MAXVALS], valid, runs, invers;
    valid = read_int_array(numbers, MAXVALS);
    
    /* ascending runs func */
    runs = asc_runs(numbers, valid);
    printf("Number of ascending runs : %d\n", runs);
    
    /* inversions */
    invers = inversions(numbers, valid);
    printf("Number of inversions     : %d\n", invers);
        
    
    return 0;
}

int
read_int_array(int A[], int maxvals) {
	int n, excess, next;
	printf("Enter as many as %d values, ^D to end\n",
           maxvals);
	n = 0; excess = 0;
	while (scanf("%d", &next)==1) {
		if (n==maxvals) {
			excess = excess+1;
		} else {
			A[n] = next;
			n += 1;
		}
	}
	printf("%d values read into array", n);
	if (excess) {
		printf(", %d excess values discarded", excess);
	}
	printf("\n");
	return n;
}

int
asc_runs(int A[], int n) {
    int runs=1, i;
    /* runs a loop through the array */
    for (i=0;i<n-1;i++) {
        /* if the value dropped, it's a new run */
        if (A[i] > A[i+1]) {
            runs++;
        }
    }

    return runs;
}

int
inversions(int A[], int n) {
    int invers=0, i, j;
    /* loop through the array */
    for (i=0;i<n-1;i++) {
        /* if the value dropped, there must be at least one out of order */
        if (A[i] > A[i+1]) {
            printf("A[i] = %d, i = %d\n", A[i], i);
            /* invers++; */
            /* checks how far the number is out of order */
            for(j=i;j>=0;j--) {
                if (A[i+1] < A[j]) {
                    invers++;
                    printf("    A[i+1]= %d, A[j]= %d, invers= %d\n", A[i+1], A[j], invers);
                }
            }
        }
    }
    
    return invers;
}
