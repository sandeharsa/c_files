/* Calculate the volume of a sphere given its radius.
 */
#include <stdio.h>

#define PI 3.14159

int
main(int argc, char **argv) {
	float radius, volume;
	printf("Enter sphere radius: ");
	scanf("%f", &radius);
	volume = (4.0*PI*radius*radius*radius)/3.0;
	printf("When radius i   s %.2f meters, ", radius);
	printf("volume is %.2f cubic meters\n", volume);
	return 0;
}

/* =====================================================================
 Program written by Alistair Moffat, as an example for the book
 "Programming, Problem Solving, and Abstraction with C", Pearson
 SprintPrint, Sydney, Australia, 2003.
 
 See http://www.csse.unimelb.edu.au/~alistair/ppsaa/ for further
 information.
 
 This version prepared January 2012.
 ================================================================== */
