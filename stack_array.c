/*
//  stack_array.c
//  
//
//  Created by Sande Harsa on 10/8/12.
//
*/

#include <stdio.h>
#include <stdlib.h>

#define SIZE 100

/* typedef(s) */
typedef struct {
    int i;
} data_t;

typedef struct {
    data_t data[SIZE];
    size_t array_size;
    int ndata;
} stacka_t;

/* function prototypes */
stacka_t *make_empty_stack(stack_t *p);
stacka_t *push(stack_t *p, data_t stuff);
stacka_t *pop(stack_t *p);

int
main(int argc, char **argv) {
    stacka_t *p;
    data_t stuff;
    stuff.i = 0;
    p = make_empty_stack(p);
    p = push(p, stuff);
    printf("%d\n", p->data[0].i);
    return 0;
}

stacka_t
*make_empty_stack(stacka_t *p) {
    p->data = malloc(SIZE*sizeof(data_t));
    p->ndata = 0;
    p->array_size = 0;
    return p;
}

stacka_t
*push(stacka_t *p, data_t stuff) {
    /* realloc memory space if the current allocation is full */
    if(p->array_size==SIZE) {
        p->array_size *= 2;
        p = realloc(p, p->array_size*sizeof(p));
    }
    /* move items that is already inside the array one step */
    for(n=p->ndata;n>=0;n--) {
        p->data[n+1] = p->data[n]
    }
    /* insert the new data */
    p->data[0] = stuff;
    /* update variable(s) */
    ndata += 1;
    return p;
}

