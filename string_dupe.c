/*
//  string_dupe.c
//  Creates a copy of a string and then return a pointer to it.
//
//  Created by Sande Harsa on 9/30/12.
//
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#define MAX_WORD_LEN 23
#define MAX_SET_LEN 100

typedef char word_t[MAX_WORD_LEN+1];

char* string_dupe(char*);
char** string_set_dupe(char**);
void string_set_free(char**);
void print_set(char**);

int
main(int argc, char **argv) {
    int i;
    char **S, **Scpy;
    S = malloc((argc-1)*sizeof(*S));
    assert(S);

    printf("Your input      : ");
    for (i=1;i<argc;i++) {
        printf("%s ", argv[i]);
        S[i-1] = malloc(strlen(argv[i])*sizeof(char));
        assert(S[i-1]);
        strcpy(S[i-1], argv[i]);
    }
    S[i-1] = NULL;
    printf("\n");

    printf("Copied string   : ");
    print_set(Scpy=string_set_dupe(S));
    string_set_free(S);
    string_set_free(Scpy);	
    return 0;
}

char*
string_dupe(char *s) {
    char *scpy;
    scpy = malloc(strlen(s)*sizeof(char));
    strcpy(scpy, s);
    return scpy;
}

char**
string_set_dupe(char **S) {
    int i;
    char **setcpy;
    setcpy = malloc(MAX_SET_LEN*sizeof(*setcpy));
    for(i=0;S[i]!=NULL;i++) {
        setcpy[i] = string_dupe(S[i]);
    }
    setcpy[i] = NULL;
    return setcpy;
}

void
string_set_free(char **S) {
    free(S);
    S = NULL;
}

void
print_set (char **S) {
    int i;
    for (i=0; S[i]!=NULL;i++) {
        printf("%s ",S[i]);
    }
    printf("\n");
}
