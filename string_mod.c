/*
//  string_mod.c
//  Analyze and modify strings on C.
//
//  Created by Sande Harsa on 9/2/12.
//
*/

#include <stdio.h>
#include <string.h>

#define MAXCHAR 50

int is_subsequence(char*, char*);
int is_subsequence1(char*, char*);
int is_subset(char*, char*);
int is_anagram(char*, char*);

int
main(int argc, char **argv) {
    /*char string1[MAXCHAR], string2[MAXCHAR];
    printf("Enter the two strings: ");
    scanf("%80[^\n] %80[^\n]", string1, string2);
    printf("The entered strings are: %s %s\n", string1, string2);
    gets(string1);
    gets(string2);*/
    if (is_subsequence1(argv[1], argv[2])) {
        printf("The two strings are in subsequence.\n");
    } else {
        printf("The two strings are NOT in subsequence.\n");
    }
    if (is_subset(argv[1], argv[2])) {
        printf("The first string is a subset of the second.\n");
    } else {
        printf("The first string is NOT a subset of the second.\n");
    }
    is_anagram(argv[1], argv[2]);
        
    return 0;
}

int
is_subsequence(char *s1, char *s2) {
    int n1=0, n2=0, prev_pos;
    while (s1[n1]) {
        while (s2[n2]) {
            if (s1[n1] == s2[n2]) {
                prev_pos = n2;
                n1++;
                break;
            }
            prev_pos=-1;
            n2++;
        }
        if (prev_pos==-1) {
            return 0;
        }
    }
    return 1;
}

/* Better version of is_subsequence */
int
is_subsequence1(char *s1, char *s2) {
    int n1=0, n2=0;
    while (s2[n2]) {
        if (s2[n2] == s1[n1]) {
            n1++;
        }
        n2++;
        if (!(s1[n1])) {
            return 1;
        }
    }
    return 0;
}

int
is_subset(char *s1, char *s2) {
    int n1=0, i, char_fnd=0, str_end;
    char str_check[MAXCHAR];
    strcpy(str_check,s2);
    str_end = strlen(str_check);
    while (s1[n1]) {
        for (i=0;i<str_end;i++) {
            if (s1[n1]==str_check[i]) {
                char_fnd=1;
                str_check[i]='\0';
                break;
            }
        }
        if (!char_fnd) {
            return 0;
        }
        char_fnd=0;
        n1++;
    }
    return 1;
}

int
is_anagram(char *s1, char*s2) {
    char cs1[MAXCHAR], cs2[MAXCHAR];
    
    strcpy(cs2, s2);
    
    
    return 0;
}

