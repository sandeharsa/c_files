/*
//  string_mod.c
//  Analyze and modify strings on C.
//
//  Created by Sande Harsa on 9/2/12.
//
*/

#include <stdio.h>

int is_subsequence(char*, char*);

int
main(int argc, char **argv) {
    char *string1, *string2;
    string1 = "test";
    string2 = "testing";
    /* printf("Enter the two strings: ");
    scanf("%s %s", string1, string2);
    printf("The entered strings are: %s and %s", string1, string2); */
    if (is_subsequence(string1, string2)) {
        printf("The two strings are in subsequence.\n");
    } else {
        printf("The two strings are NOT in subsequence.\n");
    }
        
    return 0;
}

int
is_subsequence(char *s1, char *s2) {
    int n1, n2, prev_pos;
    while (s1[n1]) {
        while (s2[n2]) {
            if (s1[n1] == s2[n2]) {
                prev_pos = n2;
                n1++;
                break;
            }
            prev_pos=0;
            n2++;
        }
        if (prev_pos==0) {
            return 0;
        }
    }
    return 1;
}