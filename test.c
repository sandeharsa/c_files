/*
//  test.c
//  
//
//  Created by Sande Harsa on 8/16/12.
//
*/

#include <stdio.h>
#include <string.h>

void print();

int GLOBAL;

int
main(int argc, char **argv) {
    GLOBAL = 1;
    print();
    return 0;
}

void
print() {
    printf("%d\n", GLOBAL);
}