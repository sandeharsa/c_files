/* Explore the 3n+1 problem.
 */
#include <stdio.h>

#define PERLINE 8

int
main(int argc, char **argv) {
    
	int n, cycles, max;
	
	printf("Enter starting value for n: ");
	scanf("%d", &n);
	max = n;
	cycles = 0;
	while (n>1) {
		printf("%5d ", n);
		if (n%2==0) {
			n = n/2;
		} else {
			n = 3*n+1;
		}
		if (n>max) {
			max = n;
		}
		cycles += 1;
		if (cycles%PERLINE == 0) {
			printf("\n");
		}
	}
	printf("\n%d cycles consumed, maximum was %d\n",
           cycles, max);
	return 0;
}

/* =====================================================================
 Program written by Alistair Moffat, as an example for the book
 "Programming, Problem Solving, and Abstraction with C", Pearson
 SprintPrint, Sydney, Australia, 2003.
 
 See http://www.csse.unimelb.edu.au/~alistair/ppsaa/ for further
 information.
 
 This version prepared January 2012.
 ================================================================== */  