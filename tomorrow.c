#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char **argv){
    int date, month, year;
    /*reading input*/
    printf("Input your date in dd/mm/yy format: ");
    if (scanf( "%d/%d/%d",&date,&month, &year) != 3) {
        printf("scanf failed to read three items\n");
        exit(EXIT_FAILURE);
    }else if (date<=0 || month<=0 || month>12 || year<=1752){
        printf("Invalid Date input\n");
        exit(EXIT_FAILURE);
    }
    /*processing*/
    if (date<=27){date+=1;}
    else if (month==2){
        if (year%4==0 && (year%100!=0 || year%400==0) && date==28){
            date+=1;
        }else if (year%4==0 && (year%100!=0 || year%400==0) && date==29) {
            date=1;
            month+=1;
        }else {
            printf("Invalid Date input\n");
            exit(EXIT_FAILURE);
        }
    }else if (month == 4||month== 6||month== 9||month== 11){
        if (date<30){
            date+=1;
        }else if (date==30){
            date=1;
            month+=1;
        }else {
            printf("Invalid Date input\n");
            exit(EXIT_FAILURE);
        }
    }else if (month ==12){
        if (date==31){
            date=1;
            month=1;
            year+=1;
        }else if (date<31) {
            date+=1;
        }else {
            printf("Invalid Date input\n");
            exit(EXIT_FAILURE);
        }
    }else{
        if (date==31){
            date=1;
            month+=1;
        }else if (date<31) {
            date+=1;
        }else {
            printf("Invalid Date input\n");
            exit(EXIT_FAILURE);
        }
    }
    /*printing out result*/
    printf("tommorow's date is : %d/%d/%d", date, month, year);
    return 0;
}