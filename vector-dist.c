/*
//  vector-dist.c
//  Calculate the distance between two points
//
//  Created by Sande Harsa on 8/14/12.
//
*/

#include <stdio.h>
#include <math.h>

double distance(int, int, int, int);


int
main(int argc, char **argv) {
    int x1, y1, x2, y2;
    double dist;
    
    printf("Enter x1, y1 and x2, y2:\n");
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    
    dist = distance(x1, y1, x2, y2);
    
    printf("The distance is: %.2f\n", dist);
    
    return 0;
}

double
distance(int x1, int y1, int x2, int y2) {
    double x_vec, y_vec, dist;
    
    x_vec = x2 - x1;
    y_vec = y2 - y1;
    dist = sqrt((x_vec*x_vec)+(y_vec*y_vec));
    
    return dist;
}