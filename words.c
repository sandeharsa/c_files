/* Break the input up into a sequence of words, and only report
   the unique ones.
*/
#include <stdio.h>
#include <string.h>

#define MAXCHARS 10      /* Max chars per word */
#define MAXWORDS 1000    /* Max distinct words */

typedef char word_t[MAXCHARS+1];
int getword(word_t, int);

int
main(int argc, char **argv) {
	word_t one_word, all_words[MAXWORDS];
    int freq[MAXWORDS];
	int numdistinct=0, totwords=0, i, found;
    memset(freq,0,MAXWORDS*sizeof(int));
	while (getword(one_word, MAXCHARS) != EOF) {
		totwords = totwords+1;
		/* linear search in array of previous words... */
		found = 0;
		for (i=0; i<numdistinct && !found; i++) {
			if (strcmp(one_word, all_words[i]) == 0) {
                found = 1;
                freq[i] += 1;
            }
		}
		if (!found && numdistinct<MAXWORDS) {
			strcpy(all_words[numdistinct], one_word);
            freq[numdistinct] += 1;
			numdistinct += 1;
		}
		/* NB - program silently discards words after
		   MAXWORDS distinct ones have been found */
	}
	printf("%d words read\n", totwords);
	for (i=0; i<numdistinct; i++) {
		printf("word #%d is \"%s\", freq: %d\n", i, all_words[i], freq[i]);
	}
	return 0;
}

#include "getword.c"

/* =====================================================================
   Program written by Alistair Moffat, as an example for the book
   "Programming, Problem Solving, and Abstraction with C", Pearson
   SprintPrint, Sydney, Australia, 2003.

   See http://www.csse.unimelb.edu.au/~alistair/ppsaa/ for further
   information.

   This version prepared January 2012.
   ================================================================== */
